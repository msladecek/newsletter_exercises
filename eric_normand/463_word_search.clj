(defn found? [needle haystack]
  (let [extended-needle (->> needle
                             (interpose "[a-zA-Z0-9]*")
                             (apply str))]
    (-> (re-find (re-pattern extended-needle) haystack)
        boolean)))


;;      needle   haystack
(found? "abc" "dddabcfff") ;=> true (direct match)
(found? "abc" "xyzannbffooc") ;=> true (add missing "nn" and "ffoo")
(found? "abc" "a bc") ;=> false (don't match across whitespace)
(found? "xxx" "cxccx") ;=> false (not enough x's)
(found? "" "") ;=> true (trivially so)
(found? "" "aaa") ;=> true (also trivial)
