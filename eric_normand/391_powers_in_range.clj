(require '[clojure.test :refer [is]])

(defn powers-in-range [n a b]
  (->> (range)
       (map #(reduce * (repeat n %)))
       (drop-while #(< % a))
       (take-while #(<= % b))))

(and
  (is (= (powers-in-range 2 49 65) [49 64]))
  (is (= (powers-in-range 3 1 27) [1 8 27]))
  (is (= (powers-in-range 10 1 5) [1])))
