(require '[clojure.test :refer [are deftest is]]
         '[clojure.set :as set]
         '[clojure.string :as str])

(defn contraindications [meds pairs]
  (let [contra-sets (set (for [[left right] pairs] #{left right}))
        patient-pairs (set (for [left meds
                                 right meds
                                 :when (not (= left right))]
                             (set (map :rxNorm [left right]))))]
    (set/intersection contra-sets patient-pairs)))

(deftest test-contraindications
  (let [patient-medications [{:name "Blythenal" :rxNorm "blyth"}
                             {:name "Masbutol" :rxNorm "masbut"}
                             {:name "Welbutril" :rxNorm "welb"}]
        contraindication-pairs [["nr913ng" "blyth"]
                                ["masbut"  "87f2h139049f"]
                                ["nr913ng" "j1j81f0"]
                                ["blyth" "welb"]
                                ["masbut"  "welb"]]]
    (is (= #{#{"masbut" "welb"} #{"blyth" "welb"}}
           (contraindications patient-medications contraindication-pairs))))
  (is (= #{} (contraindications [] []))))

(test-contraindications)
