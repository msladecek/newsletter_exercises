(defn pick-shortest [path1 path2]
  (if (< (:distance path1) (:distance path2))
    path1
    path2))


(defn shortest-path [graph origin target]
  (let [neighbours (->> (keys graph)
                        (map (fn [[from to]]
                               {from #{to}}))
                        (apply merge-with clojure.set/union))]
    (loop [paths {origin {:distance 0 :path [origin]}}
           unvisited (->> (keys graph)
                          (apply concat)
                          (into #{}))]
      (let [reached (paths target)
            available-paths (filter (comp unvisited key) paths)]
        (cond
          reached (:path reached)
          (not (seq available-paths)) nil
          :else
          (let [[current-node current-path] (apply min-key (comp :distance val) available-paths)
                tenative-paths (->> (neighbours current-node)
                                    (map (fn [node]
                                           [node {:distance (+ (:distance current-path)
                                                               (graph [current-node node]))
                                                  :path (conj (:path current-path) node)}]))
                                    (into {}))]
            (recur (merge-with pick-shortest paths tenative-paths)
                   (disj unvisited current-node))))))))



(def graph {[:a :b] 1
            [:a :c] 2
            [:c :a] 4})

(shortest-path graph :c :b) ;=> [:c :a :b]
(shortest-path graph :a :a) ;=> [:a]
(shortest-path graph :a :b) ;=> [:a :b]
(shortest-path graph :b :c) ;=> nil (because no path exists)
