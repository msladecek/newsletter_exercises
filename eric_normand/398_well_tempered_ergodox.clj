(defn note [string fret]
  (if-not (or (<= 1 string 6)
              (<= 1 fret 24))
    ::incorrect-input
    (let [strings
          (let [note-sequence (cycle [:F :F#-Gb :G :G-Ab :A :A#Bb :B :C :C#-Db :D :D#-Eb :E])
                offsets (reductions + [0 7 8 7 7 7])]
            (mapv
             (fn [off]
               (->> note-sequence (drop off) (take 24) vec))
             offsets))]
      (get-in strings [(dec string) (dec fret)]))))

(note 6 24)
