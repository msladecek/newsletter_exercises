(defn all-present? [bigrams string]
  (every? #(re-find (re-pattern %) string) bigrams))


(all-present? ["st" "tr"] "street") ;=> true
(all-present? ["ea" "ng" "kt"] "eating at a restaurant") ;=> false
(all-present? [] "hello!") ;=> true
