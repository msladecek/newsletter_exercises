(comment
  "If you iteratively apply the square root, you will eventually get a number strictly less than two.
Write a function that figures out how many times you have to apply the square root it results in a number less than two.")


(defn iter-sqrt [n]
  (loop [iter-count 0 n n]
    (if (< n 2)
      iter-count
      (recur (inc iter-count) (Math/sqrt n)))))
