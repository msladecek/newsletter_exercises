;; Your job is to take a month (keyword) day (number) and a hemisphere (:north or :south) and determine which season it is (return a keyword), according to this handy table.

;; Start       End         North  South
;; March 1     May 31      Spring Autumn
;; June 1      August 31   Summer Winter
;; September 1 November 30 Autumn Spring
;; December 1  February 29 Winter Summer

;; Example:

;; (which-season :north :march 5) ;=> :spring
;; (which-season :south :december 25) ;=> :summer


(def month->int (zipmap [:january :february :march :april :may :june :july :august :september :october :november :december]
                        (range)))

(defn date<= [& dates]
  (->> dates
       (map (fn [[month day]] [(month->int month) day]))
       (partition 2 1)
       (map #(apply compare %))
       (every? #(<= % 0))))

(def seasons-astronomical
  (reify clojure.lang.ILookup
    (valAt [this date]
      (cond
        (date<= [:march     21] date [:june      20]) {:north :spring :south :autumn}
        (date<= [:june      21] date [:september 22]) {:north :summer :south :winter}
        (date<= [:september 23] date [:december  20]) {:north :autumn :south :spring}
        (or (date<= [:december 21] date [:december 31])
            (date<= [:january   1] date [:march    20])) {:north :winter :south :summer}))))


(def seasons-meteorological
  (reify clojure.lang.ILookup
    (valAt [this [month _]]
      (let [by-season {#{:march     :april   :may}      {:north :spring :south :autumn}
                       #{:june      :july    :august}   {:north :summer :south :winter}
                       #{:september :october :november} {:north :autumn :south :spring}
                       #{:december  :january :february} {:north :winter :south :summer}}
            by-month (into {}
                           (mapcat
                            (fn [[months by-hemisphere]]
                              (for [month months]
                                [month by-hemisphere]))
                            by-season))]
        (by-month month)))))

(defn which-season
  ([hemisphere month day]
   (get-in seasons-meteorological [[month day] hemisphere]))
  ([seasons hemisphere month day]
   (get-in seasons [[month day] hemisphere])))


(which-season :north :march 5) ;=> :spring
(which-season :south :december 25) ;=> :summer

(which-season seasons-astronomical :north :march 5) ;=> :winter
(which-season seasons-astronomical :south :december 25) ;=> :summer
