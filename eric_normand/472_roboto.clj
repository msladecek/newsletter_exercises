(defn turn-clockwise [[dir-x dir-y]]
  [dir-y (- dir-x)])

(defn move [steps]
  (let [final-state
        (reduce
         (fn [{:keys [position direction]} step]
           {:position (->> (map #(* step %) direction)
                           (mapv + position))
            :direction (turn-clockwise direction)})
         {:position [0 0]
          :direction [0 1]}
         steps)]
    (:position final-state)))


(move []) ;=> [0 0] ;; No motion
(move [10]) ;=> [0 10] ;; move 10 straight north
(move [10 2]) ;=> [2 10]
(move [10 2 3]) ;=> [2 7]
