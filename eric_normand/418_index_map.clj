(defn index-map [seq-]
  (reduce-kv
   (fn [map- key- value]
     (update map- value (fnil conj #{}) key-))
   {}
   seq-))

(defn reverse-index-map [index-map-]
  (if-not (seq index-map-)
    []
    (let [reverse-mapping
          (reduce
           (fn [map- [value indexes]]
             (reduce
              (fn [map-- index]
                (assoc map-- index value))
              map-
              indexes))
           {}
           index-map-)
          max-index (apply max (keys reverse-mapping))]
      (mapv reverse-mapping (range (inc max-index))))))


(index-map []) ;=> {}
(reverse-index-map {}) ;=> []

(index-map [1 2 3]) ;=> {1 #{0} 2 #{1} 3 #{2}}
(reverse-index-map {1 #{0} 2 #{1} 3 #{2}}) ;=> [1 2 3]

(index-map [1 1 1]) ;=> {1 #{0 1 2}}
(reverse-index-map {1 #{0 1 2}}) ;=> [1 1 1]

(index-map [1 2 1 2 1]) ;=> {1 #{0 2 4} 2 #{1 3}}
(reverse-index-map {1 #{0 2 4} 2 #{1 3}}) ;=> [1 2 1 2 1]
