(defn friday? [date]
  (= java.time.DayOfWeek/FRIDAY (.getDayOfWeek date)))


(defn next-friday-13th []
  (let [today (java.time.LocalDate/now)
        date (-> today
                 (cond-> (< 13 (.getDayOfMonth today)) (.plusMonths 1))
                 (.withDayOfMonth 13))]
    (->> date
         (iterate #(.plusMonths % 1))
         (filter friday?)
         first)))


(defn friday-13ths-in-year [year]
  (->> (range 1 13)
       (map #(java.time.LocalDate year % 13))
       (filter friday?)))


(defn friday-13th?
  ([year month]
   (friday-13th? year month 13))
  ([year month day]
   (let [date (java.time.LocalDate/of year month day)]
     (and (= 13 day)
          (friday? date)))))


(defn next-friday-13th-in-month [month]
  (let [next-friday-13th-instance (next-friday-13th)]
    (= month (.getMonth next-friday-13th-instance))))
