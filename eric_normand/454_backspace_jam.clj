(defn apply-bs [string]
  (->> string
       (reduce
        (fn [chs ch]
          (cond
            (and (seq chs) (= \# ch)) (pop chs)
            (= \# ch) chs
            :else (conj chs ch)))
        [])
       (apply str)))

(apply-bs "abc#") ;=> "ab"
(apply-bs "abc###") ;=> ""
(apply-bs "###abc") ;=> "abc"
(apply-bs "there###eir") ;=> "their"
