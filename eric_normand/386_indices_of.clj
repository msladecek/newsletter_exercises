(defn indices-of [value seq-]
  (keep-indexed (fn [idx el] (when (= el value) idx)) seq-))


(indices-of :d [:a 1 :d :f :r 4 :d])
