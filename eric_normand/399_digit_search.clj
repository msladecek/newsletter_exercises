(defn int->digit-set [num]
  (loop [num num
         digits #{}]
    (if (zero? num)
      digits
      (recur (quot num 10)
             (conj digits (mod num 10))))))


(defn digit-search [nums]
  (loop [nums nums
         digits (set (range 10))]
    (let [[num & nums-next] nums
          digits-next (clojure.set/difference digits (int->digit-set num))]
      (cond
        (not (seq digits-next)) num
        (nil? nums-next) nil
        :else (recur nums-next digits-next)))))


(digit-search [5175 4538 2926 5057 6401 4376 2280 6137]) ;=> 5057
;; digits found: 517- 4-38 29-6 -0

(digit-search [5719 7218 3989 8161 2676 3847 6896 3370]) ;=> 3370
;; digits found: 5719 -2-8 3--- --6- ---- --4- ---- ---0

(digit-search [4883 3876 7769 9846 9546 9634 9696 2832]) ;=> nil
;; digits found: 48-3 --76 ---9 ---- -5-- ---- ---- 2---
;; 0 and 1 are missing
