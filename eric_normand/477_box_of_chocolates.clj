(defn assemble [smalls larges mass]
  (let [num-larges (min larges (quot mass 5))
        num-smalls (min smalls (quot (- mass (* 5 num-larges)) 2))]
    (when (= mass (+ (* 5 num-larges) (* 2 num-smalls)))
      {:large num-larges :small num-smalls})))


(assemble 100 100 2) ;=> {:small 1 :large 0}
(assemble 100 100 1) ;=> nil
(assemble 100 100 10) ;=> {:small 0 :large 2}
(assemble 10 2 20) ;=> {:large 2 :small 5}
