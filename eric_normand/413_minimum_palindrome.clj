(defn overlap-matches [& colls]
  (every? #(apply = %) (apply map vector colls)))


(defn ->palindrome [s]
  (let [revs (reverse s)]
    (loop [offset 0]
      (if (overlap-matches revs (drop offset s))
        (apply str (concat s (drop (- (count s) offset) revs)))
        (recur (inc offset))))))


(->palindrome "race") ; => "racecar"
(->palindrome "mad") ;=> "madam"
(->palindrome "mirror") ;=> "mirrorrim"
