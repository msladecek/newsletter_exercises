(require '[clojure.math :as math])

(defn order [num]
  (if (zero? num)
    1
    (inc (int (math/log10 num)))))

(defn num-digits [start end]
  (->> (range (inc start) end)
       (map order)
       (reduce +)))

(num-digits 0 1) ;=> 0 (there are no integers between 0 and 1)
(num-digits 0 9) ;=> 8
(num-digits 0 10) ;=> 9 (1, 2, 3, 4, 5, 6, 7, 8, 9)
(num-digits 9 100) ;=> 180
