(defn nest [elements repeat-count]
  (map #(repeat repeat-count %) elements))


(nest [:a :b :c] 2) ;=> ((:a :a) (:b :b) (:c :c))
(nest [] 10) ;=> ()
(nest [1 2 3 4] 1) ;=> ((1) (2) (3) (4))
(nest [1 2 3] 0) ;=> (()()())
