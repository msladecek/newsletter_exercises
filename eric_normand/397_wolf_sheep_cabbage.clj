(defn maybe-conj [coll item]
  (if-not (nil? item)
    (conj coll item)
    coll))


(defn apply-move [state {:keys [direction passenger]}]
  (case direction
    :across (-> state
                (update :right maybe-conj passenger)
                (update :left disj passenger)
                (assoc :ferryman-position :right))
    :return (-> state
                (update :left maybe-conj passenger)
                (update :right disj passenger)
                (assoc :ferryman-position :left))))


(defn possible-moves [state]
  (let [direction {:left :across :right :return}
        ferryman-position (:ferryman-position state)
        other-bank ({:left :right :right :left} ferryman-position)]
    (->> (conj (state ferryman-position) nil)
         (map (fn [passenger]
                {:direction (direction ferryman-position)
                 :passenger passenger})))))


(defn success? [{:keys [ferryman-position right]}]
  (and (= :right ferryman-position)
       (= #{:wolf :sheep :cabbage} right)))


(defn failure? [{:keys [ferryman-position] :as state}]
  (let [incompatible-pairs #{#{:wolf :sheep} #{:sheep :cabbage}}
        other-bank (-> ferryman-position {:left :right, :right :left} state)]
    (some #(clojure.set/subset? % other-bank) incompatible-pairs)))


(defn wolf-sheep-cabbage
  ([state]
   (wolf-sheep-cabbage state [] []))
  ([{:keys [ferryman-position right left] :as state} states-so-far moves-so-far]
   (if (success? state)
     [moves-so-far]
     (apply concat
            (for [move (possible-moves state)
                  :let [new-state (apply-move state move)]
                  :when (not (or (some #{new-state} states-so-far)
                                 (failure? new-state)))]
              (wolf-sheep-cabbage new-state (conj states-so-far state) (conj moves-so-far move)))))))


(def initial-state
  {:left #{:wolf :sheep :cabbage}
   :right #{}
   :ferryman-position :left})


(pprint
 (wolf-sheep-cabbage initial-state))
