(require '[clojure.spec.alpha :as s]
         '[clojure.spec.test.alpha :as stest])

(s/def ::cell (s/or :filled #{:x :o}
                    :empty nil?))

(s/def ::board-row (s/and #(= 3 (count %))
                          (s/coll-of ::cell)))

(s/def ::board (s/and #(= 3 (count %))
                      (s/coll-of ::board-row)))

(s/fdef winner
  :args (s/cat :board ::board)
  :ret #{:x :o :draw})


(defn- cell-groups [board]
  (let [rows board
        columns (apply map vector board)
        main-diag-indices [[0 0] [1 1] [2 2]]
        side-diag-indices [[0 2] [1 1] [2 0]]
        diagonals (for [diag-indices [main-diag-indices side-diag-indices]]
                    (for [cell-indices diag-indices]
                      (get-in board cell-indices)))]
    (concat rows columns diagonals)))

(defn winner [board]
  (let [all-same-group (->> (cell-groups board)
                            (filter #(apply = %))
                            first)]
    (if-not (nil? all-same-group)
      (first all-same-group)
      :draw)))

(stest/instrument `winner)

(winner [[:x :o :x]
         [:o :x :o]
         [:o :x :x]])  ;=> :x


(winner [[:o :o :o]
         [:o :x :x]
         [nil :x :x]])  ;=> :o


(winner [[:x :x :o]
         [:o :o :x]
         [:x :x :o]])  ;=> :draw
