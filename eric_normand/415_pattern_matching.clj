(defn pattern [values pat]
  (when (= (count values) (count pat))
    (let [matching-locations (apply merge-with into (map (fn [loc ch] {ch #{loc}}) (range) pat))
          matching-values (for [[ch locs] matching-locations
                           :when (apply = (map values locs))]
                       [ch (get values (first locs))])]
      (when (seq matching-values)
        (into {} matching-values)))))


(defn pattern [values pat]
  (when (= (count values) (count pat))
    (reduce (fn [answer [letter number]]
              (if (and (contains? answer letter)
                       (not= number (get answer letter)))
                (reduced nil)
                (assoc answer letter number)))
            {}
            (map vector pat values))))

(pattern [1 2 1 2] "ABAB") ;=> {\A 1 \B 2}
(pattern [1 2 3 2] "ABCB") ;=> {\A 1 \B 2 \C 3}
(pattern [1 2 3 4] "ABAB") ;=> nil
(pattern [1 1 1 1] "A") ;=> nil (wrong number of elements)
(pattern [1 1 1 1] "ABCD") ;=> {\A 1 \B 1 \C 1 \D 1}
(pattern [1 4 2 1] "ADCA") ;=> {\A 1 \D 4 \C 2}
