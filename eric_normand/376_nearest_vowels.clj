(defn nearest-vowels [string]
  (let [vowel? (set "aeiou")
        vowels-positions (keep-indexed #(if (vowel? %2) %1) string)
        distance-to-nearest-vowel (fn [char-pos]
          (apply min (for [vowel-pos vowels-positions]
                       (Math/abs (- char-pos vowel-pos)))))]
    (mapv distance-to-nearest-vowel (range (count string)))))



(nearest-vowels "aeiou")
(nearest-vowels "babbb")
(nearest-vowels "babbba")
