(defn quadratic-roots [[a b c]]
  (when-not (zero? a)
    (let [disc (- (* b b) (* 4 a c))
          left {:real (/ (- b) (* 2 a))
                :imaginary 0}
          right (if (neg? disc)
                  {:real 0
                   :imaginary (/ (Math/sqrt (- disc)) (* 2 a))}
                  {:real (/ (Math/sqrt disc) (* 2 a))
                   :imaginary 0})]
      [(merge-with + left right)
       (merge-with - left right)])))
