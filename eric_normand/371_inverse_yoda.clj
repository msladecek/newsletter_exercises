(require '[clojure.string :as str]
         '[clojure.test :refer [are deftest]])

(defn apply-to-first-char [fun str1]
  (let [[fst & rst] str1
        capital-fst (fun fst)]
    (str capital-fst (apply str rst))))

(defn deyoda-sentence [sentence]
  (let [[first-half second-half] (str/split sentence #", " 2)]
    (if second-half
      (str/join " " [(apply-to-first-char #(Character/toUpperCase %) second-half)
                     (apply-to-first-char #(Character/toLowerCase %) first-half)])
      first-half)))

(defn ayoda [str1]
  (->> (str/split str1 #"\. ?")
       (map deyoda-sentence)
       (map #(str % \.))
       (str/join " ")))

(deftest test-ayoda
  (are [in out] (= out (ayoda in))
    "Talk like this, Yoda does.", "Yoda does talk like this."
    "Translate back to normal, you must", "You must translate back to normal."
    "Fun, Clojure is. Learn it, I will.", "Clojure is fun. I will learn it."
    "Do or do not. There is no try.", "Do or do not. There is no try."))
