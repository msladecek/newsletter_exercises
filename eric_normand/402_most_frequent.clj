(defn most-frequent [items]
  (when (seq items)
    (let [counts (reduce
                  (fn [counts item]
                    (update counts item (fnil inc 0)))
                  {}
                  items)]
      (key (apply max-key val counts)))))


(most-frequent [2 2 3 4 4 2 1 1 3 2]) ;=> 2
(most-frequent []) ;=> nil
(most-frequent [1 1 4 4 5]) ;=> 4
