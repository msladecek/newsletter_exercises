(require '[clojure.test :refer [are deftest]]
         '[clojure.string :as str])


(defn segmentations [concatenated possible-words]
  (for [word possible-words
        :when (str/starts-with? concatenated word)]
    (str/join " " (into [word] (segmentations (subs concatenated (count word)) possible-words)))))


(deftest test-segmentations
  (are [concatenated words results] (= results (segmentations concatenated words))
    "hellothere" ["hello" "there"] '("hello there")
    "fdsfsfdsjkljf" ["the" "he" "she" "it"] '()))
