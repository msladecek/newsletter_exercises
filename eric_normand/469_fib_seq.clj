(defn fib-seq
  ([] (fib-seq + 1 1))
  ([f el0 el1]
   (concat
    [el0]
    (->> {:value el1 :prev-value el0}
         (iterate (fn [{:keys [value prev-value]}]
                    {:value (f value prev-value) :prev-value value}))
         (map :value)))))


(take 10 (fib-seq)) ;=> (1 1 2 3 5 8 13 ....)
(take 10 (fib-seq + 1 1)) ;=> (1 1 2 3 5 8 13 ....)
(take 5 (fib-seq str "X" "O")) ;=> ("X" "O" "OX" "OXO" "OXOOX")
(take 6 (fib-seq * 1 1)) ;=> (1 1 1 1 1 1)
(take 8 (fib-seq * 1 2)) ;=> (1 2 2 4 8 32 256 8192)
(take 8 (fib-seq * 1 -1)) ;=> (1 -1 -1 1 -1 -1 1 -1)
(take 5 (fib-seq vector [] [])) ;=> ([] [] [[] []] [[[] []] []] [[[[] []] []] [[] []]])
