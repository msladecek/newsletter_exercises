(defn reverse-words [sentence]
  (->> (clojure.string/split sentence #"\s+")
       reverse
       (interpose " ")
       (apply str)))

(reverse-words "my name is Eric.") ;;=> "Eric. is my name"
(reverse-words "hello") ;;=> "hello"
(reverse-words "I love you") ;;=> "you love I"
