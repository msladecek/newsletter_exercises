(let [alphabet "abcdefghijklmnopqrstuvwxyz"
      rev-alphabet (apply str (reverse alphabet))]
  (def atbach-char-table
    (zipmap (concat alphabet (clojure.string/upper-case alphabet))
            (concat rev-alphabet (clojure.string/upper-case rev-alphabet)))))


(defn atbash [string]
  (->> string
       (map #(get atbach-char-table % %))
       (apply str)))


(atbash "") ;=> ""
(atbash "hello") ;=> "svool"
(atbash "Clojure") ;=> "Xolqfiv"
(atbash "Yo!") ;=> "Bl!"
