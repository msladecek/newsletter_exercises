(defn factorial [num]
  (loop [n 2, fact 1]
    (if (<= n num)
      (recur (inc n) (* n fact))
      fact)))

(defn binomial-coefficient [n k]
  (/ (factorial n)
     (* (factorial k) (factorial (- n k)))))

(defn sums-of-pairs [nums target]
  (let [half-target (/ target 2)

        [smaller equal bigger]
        (reduce
         (fn [[smaller equal bigger] num]
           (case (compare num half-target)
             -1 [(conj smaller num) equal bigger]
              0 [smaller (conj equal num) bigger]
              1 [smaller equal (conj bigger num)]))
         [[] [] []]
         nums)

        complements (frequencies bigger)

        matched-complement
        (mapcat (fn [num]
                  (let [match (- target num)
                        match-count (get complements match 0)]
                    (repeat match-count [num match])))
                smaller)

        matched-self
        (repeat (binomial-coefficient (count equal) 2) [half-target half-target])]
    (vec (concat matched-complement matched-self))))

(sums-of-pairs [2 4 5 6] 8) ;=> [[2 6]]
(sums-of-pairs [3 2 0 1 1 5] 5) ;=> [[2 3] [0 5]]
(sums-of-pairs [1 3 2 3 4 5] 7) ;=> [[2 5] [3 4] [3 4]]

(sums-of-pairs [3 4 5] 8) ;=> [[3 5]]
(sums-of-pairs [3 4 4 5] 8) ;=> [[3 5] [4 4]]
(sums-of-pairs [3 4 4 4 5] 8) ;=> [[3 5] [4 4] [4 4] [4 4]]





(require
 '[clojure.core.logic :as logic]
 '[clojure.core.logic.fd :as fd])

(defn sums-of-pairs [nums target]
  (logic/run* [a b]
    (logic/membero a nums)
    (logic/membero b nums)
    (fd/eq
     (< a b)
     (= target (+ a b)))))

(sums-of-pairs [2 4 5 6] 8) ;=> ([2 6])
(sums-of-pairs [3 2 0 1 1 5] 5) ;=> ([2 3] [0 5])
(sums-of-pairs [1 3 2 3 4 5] 7) ;=> ([3 4] [2 5] [3 4])

(sums-of-pairs [3 4 5] 8) ;=> ([3 5])
(sums-of-pairs [3 4 4 5] 8) ;=> ([3 5])
(sums-of-pairs [3 4 4 4 5] 8) ;=> ([3 5])
