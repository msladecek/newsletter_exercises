(defn deal-max [cnt items]
  (if (<= (count items) cnt)
    (mapv vector items)
    (let [total-count (count items)
          step (Math/ceil (/ total-count cnt))
          splat (partition-all step items)
          longest-split (->> splat
                             (map count)
                             (apply max))]
      (->> splat
           (map #(concat % (repeat (- longest-split (count %)) nil)))
           (apply map vector)
           (mapv #(filterv (complement nil?) %))))))


(defn deal-out [cnt items]
  (if (< (count items) cnt)
    (throw
     (ex-info "Too few items provided" {:sequence-count cnt :item-count (count items)}))
    (let [splat (partition-all cnt items)
          longest-split (->> splat
                             (map count)
                             (apply max))]
      (->> splat
           (map #(concat % (repeat (- longest-split (count %)) nil)))
           (apply map vector)
           (mapv #(filterv (complement nil?) %))))))


;; at most 3 elements in each subsequence
(deal-max 3 [1 2 3 4 5 6 7 8]) ;=> [[1 4 7] [2 5 8] [3 6]]
(deal-max 3 [1 2]) ;=> [[1] [2]]
(deal-max 3 [1 2 3 4]) ;=> [[1 3] [2 4]]

;; deal out 4 subsequences
(deal-out 4 [1 2 3 4 5 6 7 8]) ;=> [[1 5] [2 6] [3 7] [4 8]]
(deal-out 4 [1 2]) ;=> error
(deal-out 4 [1 2 3 4]) ;=> [[1] [2] [3] [4]]
