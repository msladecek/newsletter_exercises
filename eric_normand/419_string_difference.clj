(defn strdiff [a b]
  (let [counts-a (frequencies a)
        counts-b (frequencies b)]
    (reduce-kv (fn [leftovers character char-count]
                 (let [new-val (- (leftovers character 0) char-count)]
                   (if (<= new-val 0)
                     (dissoc leftovers character)
                     (assoc leftovers character new-val))))
               counts-b
               counts-a)))


(strdiff "abc" "") ;=> {} ;; no characters in b don't occur in a
(strdiff "abc" "abc") ;=> {} ;; ditto
(strdiff "" "abc") ;=> {\a 1 \b 1 \c 1}
(strdiff "axx" "abcc") ;=> {\b 1 \c 2}
(strdiff "xxxx" "xxxxxx") ;=> {\x 2} ;; two x's in b that don't occur in a
