(require '[clojure.string :as str])


(defn first-before? [text char-a char-b]
  (let [low-text (str/lower-case text)
        last-a (str/last-index-of low-text (first (str/lower-case char-a)))
        first-b (str/index-of low-text (first (str/lower-case char-b)))]
    (< last-a first-b)))


(first-before? "A rabbit jumps joyfully" \a \j) ;=> true
(first-before? "A jolly rabbit jumps joyfully" \a \j) ;=> false
(first-before? "Don't tread on me" \t \m) ;=> true
(first-before? "Every React podcast" \t \d) ;=> false
