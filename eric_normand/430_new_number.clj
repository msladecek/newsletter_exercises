(defn num->digits [num]
  (loop [num num
         digits (list)]
    (let [q (quot num 10)
          r (rem num 10)]
      (if (< 0 q)
        (recur q (conj digits r))
        (conj digits r)))))


(defn new-number? [num]
  (let [digits (num->digits num)]
    (apply <= digits)))


(new-number? 789) ;=> true

(new-number? 645) ;=> false

(new-number? 444) ;=> true (it's permutations are not smaller than it)



(require '[clojure.math.combinatorics :as combo])


(defn digits->num [digits]
  (reduce (fn [acc digit] (+ digit (* 10 acc))) digits))


(defn new-number?-2 [num]
  (->> (num->digits num)
       combo/permutations
       (map digits->num)
       (every? #(<= num %))))


(new-number?-2 789) ;=> true

(new-number?-2 645) ;=> false

(new-number?-2 444) ;=> true (it's permutations are not smaller than it)
