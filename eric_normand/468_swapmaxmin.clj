(defn number->digits [number]
  (loop [number number
         digits (list)]
    (let [q (quot number 10)
          r (rem number 10)]
      (if (< 0 q)
        (recur q (conj digits r))
        (conj digits r)))))

(defn digits->number [digits]
  (reduce (fn [sum digit] (+ (* 10 sum) digit)) digits))

(defn reassemble [by-location]
  (map by-location (range (count by-location))))

(defn swap-target-to-the-front [by-location target-digit decide-fn]
  (let [locations (->> by-location
                           (filter (fn [[_loc digit]]
                                     (= digit target-digit)))
                           (map (fn [[loc _digit]]
                                  loc)))
        swaps (->> locations
                   (map (fn [loc]
                          (assoc by-location
                                 0 target-digit
                                 loc (by-location 0)))))]
    (->> swaps
         (map reassemble)
         (map digits->number)
         (apply decide-fn))))

(defn swapmaxmin [num]
  (let [by-location (into {} (map vector (range) (number->digits num)))
        max-digit (apply max (vals by-location))
        min-digit (apply min (filter #(not= 0 %) (vals by-location)))]
    [(swap-target-to-the-front by-location max-digit max)
     (swap-target-to-the-front by-location min-digit min)]))


(swapmaxmin 213) ;=> [312, 123]
(swapmaxmin 12345) ;=> [52341, 12345] ;; the number was already the smallest
(swapmaxmin 100) ;=> [100, 100] ;; no swap possible because of zeroes
