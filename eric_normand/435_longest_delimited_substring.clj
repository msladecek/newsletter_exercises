(defn delimited [string]
  (let [occurences
        (map (fn [pos ch] {:pos pos :char ch}) (range) string)
        substring-metas
        (->> occurences
             (group-by :char)
             (mapcat (fn [[ch ch-occurs]]
                       (->> ch-occurs
                            (partition 2 1)
                            (map (fn [[start end]]
                                   {:start (:pos start)
                                    :end (:pos end)
                                    :dist (- (:pos end) (:pos start))}))))))]
    (when (seq substring-metas)
      (let [longest (apply max-key :dist substring-metas)]
        (subs string (:start longest) (inc (:end longest)))))))


(delimited "ffdsfuiofl") ;=> "fuiof"
(delimited "abbcdefg") ;=> "bb"
(delimited "opoiifdopf") ;=> "poiifdop"
(delimited "abc") ;=> nil
