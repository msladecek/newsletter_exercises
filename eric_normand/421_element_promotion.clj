(defn promote [pred coll]
  (let [one (first coll)
        two (second coll)]
    (cond
      (nil? two) (into '() coll)
      (and (pred two) (not (pred one))) (cons two (promote pred (cons one (drop 2 coll))))
      :else (cons one (promote pred (cons two (drop 2 coll)))))))


(promote even? [1 3 5 6]) ;=> (1 3 6 5)
(promote even? []) ;=> ()
(promote even? [2 1]) ;=> (2 1)
(promote even? [0 2 4 6]) ;=> (0 2 4 6)
(promote even? [0 1 2 3 4 5 6]) ;=> (0 2 1 4 3 6 5)
(promote even? [1 2 2 2 2]) ;=> (2 2 2 2 1)
