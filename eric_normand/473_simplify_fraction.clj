(defn simplify [n d]
  (let [frac (rationalize (/ n d))]
    (if (integer? frac)
      [frac 1]
      [(numerator frac) (denominator frac)])))


;; the fraction 10/10
(simplify 10 10) ;=> [1 1]
;; the fraction 1/3
(simplify 1 3) ;=> [1 3]
(simplify 2 4) ;=> [1 2]
(simplify 100 40) ;=> [5 2]
