(defn max-sum [nums]
  (loop [max-so-far 0
         prev-sum 0
         nums nums]
    (if (not (seq nums))
      max-so-far
      (let [num (first nums)
            next-sum (+ prev-sum num)]
        (recur (max max-so-far next-sum)
               (if (< num next-sum)
                 next-sum
                 num)
               (rest nums))))))


(max-sum []) ;=> 0 ; (sum of empty seq is 0)
(max-sum [1]) ;=> 1
(max-sum [1 10]) ;=> 11
(max-sum [-1]) ;=> 0 (because you can choose the empty subsequence, which has sum 0)
(max-sum [3 -6 2 4 -2 7 -9]) ;=> 11 (sum of [2 4 -2 7])
(max-sum [8 -6 2 4 -2 -4 7 -9]) ;=> 9
