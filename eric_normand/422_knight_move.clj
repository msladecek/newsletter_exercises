(def column-symbols [:A :B :C :D :E :F :G :H])
(def column-symbol->column-id (zipmap column-symbols (range 1 9)))
(def column-id->column-symbol (zipmap (range 1 9) column-symbols))


(defn knight-move [[column-symbol row-id]]
  (let [column-id (column-symbol->column-id column-symbol)]
    (->> (for [row-step [-2 -1 1 2]
               column-step [-2 -1 1 2]
               :let [moved-row-id (+ row-id row-step)
                     moved-column-id (+ column-id column-step)]
               :when (and (not= (Math/abs row-step)
                                (Math/abs column-step))
                          (<= 1 moved-row-id 8)
                          (<= 1 moved-column-id 8))]
           [(column-id->column-symbol moved-column-id) moved-row-id])
         (into []))))


(knight-move [:D 3]) ;=> [[:B 2] [:C 1] [:B 4] [:C 5] [:E 5] [:F 4] [:E 1] [:F 2]]
(knight-move [:A 1]) ;=> [[:B 3] [:C 2]]


(def knight-deltas
  (let [distances [-2 -1 1 2]]
    (for [row-step distances
          column-step distances
          :when (not= (Math/abs row-step)
                      (Math/abs column-step))]
      [column-step row-step])))


(defn in-chessboard? [coords]
  (every? #(<= 1 % 8) coords))


(defn knight-move2 [[column-symbol row-id]]
  (let [column-id (column-symbol->column-id column-symbol)]
    (->> knight-deltas
         (map #(map + % [column-id row-id]))
         (filter in-chessboard?)
         (mapv (fn [[column-id row-id]]
                 [(column-id->column-symbol column-id) row-id])))))


(knight-move2 [:D 3]) ;=> [[:B 2] [:C 1] [:B 4] [:C 5] [:E 5] [:F 4] [:E 1] [:F 2]]
(knight-move2 [:A 1]) ;=> [[:B 3] [:C 2]]
