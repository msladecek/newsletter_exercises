(def vowels #{\a \e \i \o \u})


(defn extract-vowels [word]
  (->> (clojure.string/lower-case word)
       (into #{})
       (clojure.set/intersection vowels)))


(defn vowel-families [words]
  (->> words
       (group-by extract-vowels)
       vals
       (into [])))


(vowel-families ["hello" "vowel" "fox" "cot" "hat" "cat"]) ;=> [["hello" "vowel"]
                                                           ;    ["fox" "cot"]
                                                           ;    ["hat" "cat"]]
(vowel-families []) ;=> []
(vowel-families ["tree" "tent" "blanket"]) ;=> [["tree" "tent"]
                                           ;    ["blanket"]]
