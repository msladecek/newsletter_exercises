(defn search [document word]
  (let [lower-word-pattern
        (re-pattern (clojure.string/lower-case word))
        matching-sentences
        (->> document
             (partition-by #{\. \! \?})
             (partition 2)
             (map (fn [[sentence mark]]
                    (->> (concat sentence mark)
                         (drop-while #(= % \space))
                         (apply str))))
             (filter (fn [sentence]
                       (re-find lower-word-pattern (clojure.string/lower-case sentence)))))]
    (when (seq matching-sentences)
      (into [] matching-sentences))))


(search "This is my document." "Hello") ;=> nil
(search "This is my document. It has two sentences." "sentences") ;=> ["It has two sentences."]
(search "I like to write. Do you like to write?" "Write") ;=> ["I like to write." "Do you like to write?"]
