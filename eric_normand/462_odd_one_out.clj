(defn odd-one? [strings]
  (let [lengths (frequencies (map count strings))]
    (-> (and (= 2 (count lengths))
             (some #(= 1 %) (vals lengths)))
        boolean)))


(odd-one? ["a" "b" "c"]) ;=> false
(odd-one? ["a" "b" "cc"]) ;=> true
(odd-one? ["abc" "aa" "xyz" "jj"]) ;=> false
