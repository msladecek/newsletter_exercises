(defn occupied-positions [board]
  (->> (for [[row_id row] (map vector (range) board)
             [col_id cell] (map vector (range) row)
             :when (= 1 cell)]
         [row_id col_id])
       (into #{})))


(defn knight-moves [[r c]]
  #{[(- r 2) (- c 1)] [(- r 2) (+ c 1)] [(+ r 2) (- c 1)] [(+ r 2) (+ c 1)]
    [(- r 1) (- c 2)] [(- r 1) (+ c 2)] [(+ r 1) (- c 2)] [(+ r 1) (+ c 2)]})


(defn captures [board]
  (->> (for [knight (occupied-positions board)
             possible (knight-moves knight)]
         #{knight possible})
       frequencies
       (filter #(< 1 (val %)))
       (mapv key)))


(captures [[0 0 0 1 0 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 1 0 0 0 1 0 0]
           [0 0 0 0 1 0 1 0]
           [0 1 0 0 0 1 0 0]
           [0 0 0 0 0 0 0 0]
           [0 1 0 0 0 0 0 1]
           [0 0 0 0 1 0 0 0]]) ;=> [] ;; no captures

(captures [[0 0 0 1 0 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 0 0 0 1 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 0 0 0 0 0 0 0]]) ;=> [#{[0 3] [2 4]}] ;; one capture

(captures [[0 0 0 1 0 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 0 0 0 1 0 0 0]
           [0 0 1 0 0 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 0 0 0 0 0 0 0]
           [0 0 0 0 0 0 0 0]]) ;=> [#{[0 3] [2 4]} #{[2 4] [3 2]}] ;; two captures
