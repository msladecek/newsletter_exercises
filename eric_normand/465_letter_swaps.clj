(require '[clojure.set :as set])


(defn letter-swaps [strings target]
  (mapv
   (fn [candidate]
     (when (= (count candidate) (count target))
       (let [matchups (->> (map vector candidate target)
                           (filter (fn [[a b]] (not= a b))))
             left-matches (into #{} matchups)
             right-matches (into #{} (map reverse matchups))
             common (set/intersection left-matches right-matches)]
         (when (= 2 (count common))
           (let [[left right] (seq common)]
             (when (= left (reverse right))
               (into {} [right])))))))
   strings))


(letter-swaps ["bacd" "abdc" "abcde" "abcc" "abcd"] "abcd") ;=> [#{\a \b} #{\c \d} nil nil nil]

