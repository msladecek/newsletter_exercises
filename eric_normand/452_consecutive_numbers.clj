(defn consec [nums]
  (let [sorted (sort nums)]
    (when (->> sorted
               (partition 2 1)
               (every? (fn [[a b]] (= 1 (- b a)))))
      sorted)))


(consec []) ;=> ()
(consec [1]) ;=> (1)
(consec [3 1 2]) ;=> (1 2 3)
(consec [5 3 2 1]) ;=> nil
(consec [7 8 9 7]) ;=> nil
