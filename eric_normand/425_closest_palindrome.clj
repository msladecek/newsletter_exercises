(defn num->digits [num]
  (loop [num num
         digits (list)]
    (let [q (quot num 10)
          r (rem num 10)]
      (if (< 0 q)
        (recur q (conj digits r))
        (conj digits r)))))


(defn digits->num [digits]
  (reduce (fn [acc digit] (+ digit (* 10 acc))) digits))


(defn palindrome? [num]
  (let [digits (num->digits num)]
    (= digits (reverse digits))))


(defn closest-palindrome [num]
  (let [digits (num->digits num)
        segment-size (quot (count digits) 2)
        backward (reverse (take segment-size digits))
        base-palindrome (digits->num
                         (concat (take (- (count digits) segment-size) digits) backward))]
    (if (< base-palindrome num)
      base-palindrome
      (or (first (for [alternative (range (- (* 2 num) base-palindrome) num)
                       :when (palindrome? alternative)]
                   alternative))
          base-palindrome))))


(closest-palindrome 100) ;=> 99 (return the smaller in case of tie)
(closest-palindrome 887) ;=> 888
(closest-palindrome 888) ;=> 888
