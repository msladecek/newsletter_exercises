(defn completes? [sub string]
  (loop [sub-seq (seq sub)
         string-seq (seq string)]
    (cond
      (and (seq sub-seq)
           (empty? string-seq))
      false

      (empty? sub-seq)
      true

      (= (first sub-seq) (first string-seq))
      (recur (rest sub-seq) (rest string-seq))

      :else
      (recur sub-seq (rest string-seq)))))


(completes? "a" "autocomplete") ;=> true
(completes? "atc" "autocomplete") ;=> true
(completes? "hello" "hello") ;=> true
(completes? "ll" "hello") ;=> true
(completes? "llh" "hello") ;=> false
