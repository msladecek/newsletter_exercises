(require '[clojure.test :refer [are deftest]])


(defn uniques-1
  "my first solution, looking back at it it's needlessly complicated"
  [values]
  (let [indexed (map-indexed vector values)
        grouped (group-by second indexed)]
    (->> grouped
         (filter (fn [[value occurs]] (= 1 (count occurs))))
         (sort-by (fn [[value [[id value]]]] id))
         (map first))))

(defn uniques-2 [values]
  (let [uniques-unordered (for [[value frequency] (frequencies values)
                                :when (= 1 frequency)]
                            value)]
    (filter (set uniques-unordered) values)))

(defn uniques-3 [values]
  (let [freqs (frequencies values)]
    (filter (comp #(== 1) freqs) values)))


(deftest test-uniques
  (for [uniques [uniques-1 uniques-2 uniques-3]]
    (are [values unique-values] (= unique-values (uniques values))
      [1 2 3 4 5 6 1 2 3 5 6] '(4)
      [:a :b :c :c] '(:a :b)
      [1 2 3 1 2 3] '())))
