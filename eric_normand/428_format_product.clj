(defn format-factor-group [group]
  (let [num (first group)]
    (if (= 1 (count group))
      (str num)
      (str num "^" (count group)))))


(defn format-product [factors]
  (->> factors
       sort ; force order of factors (smallest to greatest) and grouping even non-consequent factors
       (partition-by identity)
       (map format-factor-group)
       (clojure.string/join " x ")))


(format-product [2 2 3 5]) ;=> "2^2 x 3 x 5"
(format-product [2 3 3 3 11 11]) ;=> "2 x 3^2 x 11^2"
(format-product [7]) ;=> "7"
(format-product [2 2 3 5 3]) ;=> "2^2 x 3 x 5"
