(require '[clojure.edn :as edn])


(defn rpol [expr-str]
  (let [ops #{'+ '- '* '/}]
    (reduce (fn [stack value]
              (cond
                (integer? value)
                (conj stack value)

                (ops value)
                (let [[val1 val2 & stack] stack
                      result ((eval value) val2 val1)]
                  (conj stack result))

                :else
                (ex-info "Unknown symbol" {:symbol value})))
            '()
            (edn/read-string (str "[" expr-str "]")))))


(rpol "1") ;=> 1
(rpol "1 2 +") ;=> 3
(rpol "1 2 + 3 +") ;=> 6
(rpol "4 2 * 2 2 + + 8 /") ;=> 3/2
(rpol "6 2 -") ;=> 4
