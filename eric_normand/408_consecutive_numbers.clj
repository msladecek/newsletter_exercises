(defn digits->number [digits]
  (reduce (fn [num digit] (+ (* 10 num) digit)) 0 digits))


(defn consecutive? [numbers]
  (boolean
   (reduce
    (fn [a b]
      (if (= b (inc a)) b (reduced false)))
    numbers)))


(defn consec [digits-str]
  (let [digits (map #(- (int %) (int \0)) digits-str)
        partitions (for [partition-size (range 1 (inc (quot (count digits) 2)))]
                     (partition partition-size digits))
        sequences (map #(map digits->number %) partitions)]
    (if (= 1 (count digits))
      (throw (Exception. "one digit is not enough"))
      (first (filter consecutive? sequences)))))


(consec "121314") ;=> [12 13 14]
(consec "121315") ;=> nil
(consec "444445") ;=> [444 445]
(consec "12") ;=> [1 2]
(consec "1") ; throws error
