(defn filter-indexed [pred coll]
  (->> coll
       (map-indexed vector)
       (filter #(pred (first %)))
       (map second)))

;; keep even-indexed elements
(filter-indexed even? "abcdefg") ;=> (\a \c \e \g)
