(require '[clojure.spec.alpha :as s]
         '[clojure.test :refer [are]])

(s/def ::suit #{:diamonds :hearts :spades :clubs})
(s/def ::rank (into #{:ace :king :queen :jack} (range 2 11)))
(s/def ::card (s/tuple ::rank ::suit))
(s/def ::hand (s/coll-of ::card :kind vector? :distinct true :count 5))


(defn ranks-are-consecutive? [keyfn ranks]
  (->> (map keyfn ranks)
       (sort >)
       (partition 2 1)
       (map #(apply - %))
       (apply max)
       (= 1)))

(defn score [hand]
  (let [ranks (map first hand)
        suits (map second hand)
        all-suits-same (apply = suits)
        rank-counts (vals (frequencies ranks))
        rank-count-counts (frequencies rank-counts)
        rank->int-ace-last (into {:jack 11, :queen 12, :king 13, :ace 14}
                                 (zipmap (range 2 11) (range 2 11)))
        rank->int-ace-first (assoc rank->int-ace-last :ace 1)
        straight (or (ranks-are-consecutive? rank->int-ace-first ranks)
                     (ranks-are-consecutive? rank->int-ace-last ranks))]
    (cond
      (and all-suits-same
           (= #{:ace :king :queen :jack 10} (set ranks))) :royal-flush
      (and all-suits-same
           straight) :straight-flush
      (rank-count-counts 4) :four-of-a-kind
      (and (rank-count-counts 3)
           (rank-count-counts 2)) :full-house
      all-suits-same :flush
      straight :straight
      (rank-count-counts 3) :three-of-a-kind
      (= 2 (rank-count-counts 2)) :two-pair
      (rank-count-counts 2) :pair
      :else :high-card)))

(do
  (println "--------------------------------------------------------------")
  (are [result hand] (= result (score hand))
    :three-of-a-kind [[3 :diamonds]   [3 :hearts]     [3 :spades]      [5 :hearts]     [:king :clubs]]
    :royal-flush     [[10 :hearts]    [:jack :hearts] [:queen :hearts] [:ace :hearts]  [:king :hearts]]
    :high-card       [[3 :hearts]     [5 :hearts]     [:queen :spades] [9 :hearts]     [:ace :diamonds]]
    :four-of-a-kind  [[10 :spades]    [10 :clubs]     [8 :diamonds]    [10 :diamonds]  [10 :hearts]]
    :royal-flush     [[10 :clubs]     [:ace :clubs]   [:queen :clubs]  [:jack :clubs]  [:king :clubs]]
    :straight-flush  [[:jack :hearts] [8 :hearts]     [10 :hearts]     [7 :hearts]     [9 :hearts]]
    :four-of-a-kind  [[2 :clubs]      [3 :hearts]     [2 :diamonds]    [2 :spades]     [2 :hearts]]
    :full-house      [[7 :clubs]      [4 :hearts]     [7 :spades]      [4 :diamonds]   [7 :diamonds]]
    :flush           [[:ace :hearts]  [3 :hearts]     [4 :hearts]      [:jack :hearts] [:ace :hearts]]
    :three-of-a-kind [[:ace :clubs]   [4 :hearts]     [2 :hearts]      [4 :diamonds]   [4 :spaces]]
    :two-pair        [[8 :diamonds]   [8 :clubs]      [:ace :spades]   [2 :hearts]     [:ace :hearts]]))
