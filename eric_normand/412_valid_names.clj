(require '[lambdaisland.regal :as regal]
         '[lambdaisland.regal.generator :as regal-gen])


(def r-capital-letter [:class [\A \Z]])
(def r-letter [:class [\A \Z] [\a \z]])
(def r-initial [:cat r-capital-letter "."])
(def r-word [:cat r-capital-letter [:+ r-letter]])
(def r-term [:alt r-initial r-word])
(def r-name [:cat [:+ [:cat r-term " "]] r-word])

(defn name? [s]
  (re-matches (regal/regex r-name) s))


;; Valid
(name? "George R. R. Martin")
(name? "Abraham Lincoln")
(name? "J. R. Bob Dobbs")
(name? "H. G. Wells")

;; Invalid
(name? "J R Tolkien")
(name? "J. F. K.")
(name? "Franklin")
