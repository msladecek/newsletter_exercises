(require '[clojure.test :refer [are deftest]])

(defn symmetrical? [pattern]
  (= pattern (reverse pattern)))

(defn classify [pattern]
  (let [vertical (symmetrical? pattern)
        horizontal (every? symmetrical? pattern)]
    (cond
      (and vertical horizontal) :perfect
      vertical :vertical
      horizontal :horizontal
      :else :imperfect)))

(deftest test-classify
  (are [pattern classification] (= classification (classify pattern))
    [["a" "b" "a"]
     ["x" "y" "x"]] :horizontal
    [["a" "b" "c"]] :vertical
    [["a" "b" "a"]
     ["y" "X" "y"]
     ["a" "b" "a"]] :perfect))
