(defn scroller [headline width]
  (for [i (range (dec (* 2 width)))]
    (let [extended (apply str (concat (repeat width \space)
                                      headline
                                      (repeat width \space)))
          window-start i
          window-end (+ width window-start)]
      (subs extended window-start window-end))))


(scroller "HEADLINE" 10)
