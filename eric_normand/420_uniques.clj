(defn uniques [coll]
  (let [allowed (->> (frequencies coll)
                     (filter (fn [[_ cnt]] (< cnt 2)))
                     keys
                     (into #{}))]
    (filter allowed coll)))


(uniques []) ;=> ()
(uniques [1 2 3]) ;=> (1 2 3)
(uniques [1 1 2 3]) ;=> (2 3)
(uniques [1 2 3 1 2 3]) ;=> ()
(uniques [1 2 3 2]) ;=> (1 3)
