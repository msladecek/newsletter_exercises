(defn overlap-area [[rect-a rect-b]]
  (let [point-seq [(:top-left rect-a) (:bottom-right rect-a) (:top-left rect-b) (:bottom-right rect-b)]
        [left-a right-a left-b right-b] (map :x point-seq)
        [top-a bottom-a top-b bottom-b] (map :y point-seq)
        left-inter (max left-a left-b)
        right-inter (min right-a right-b)
        top-inter (max top-a top-b)
        bottom-inter (min bottom-a bottom-b)
        horizontal-diff (max 0 (- right-inter left-inter))
        vertical-diff (max 0 (- bottom-inter top-inter))]
    (* vertical-diff horizontal-diff)))



(overlap-area [{:top-left {:x 0 :y 0}
                :bottom-right {:x 10 :y 10}}
               {:top-left {:x 5 :y 5}
                :bottom-right {:x 15 :y 15}}]) ;=> 25

;; 2 identical rectangles
(overlap-area [{:top-left {:x 0 :y 0}
                :bottom-right {:x 1 :y 1}}
               {:top-left {:x 0 :y 0}
                :bottom-right {:x 1 :y 1}}]) ;=> 1

;; no overlap
(overlap-area [{:top-left {:x 0 :y 0}
                :bottom-right {:x 1 :y 1}}
               {:top-left {:x 6 :y 6}
                :bottom-right {:x 8 :y 8}}]) ;=> 0

;; enclosing rectangles
(overlap-area [{:top-left {:x 0 :y 0}
                :bottom-right {:x 1 :y 1}}
               {:top-left {:x -1 :y -1}
                :bottom-right {:x 2 :y 2}}]) ;=> 1
