(defn gcd-euclid [a b]
  (cond
    (< a b) (gcd-euclid a (- b a))
    (> a b) (gcd-euclid (- a b) b)
    :else a))


(defn lcm2 [a b]
  (/ (* a b) (gcd-euclid a b)))

(defn lcm [nums]
  (case (count nums)
    0 1
    1 (first nums)
    2 (apply lcm2 nums)
    (reduce lcm2 nums)))

(lcm [1 2 3]) ;=> 6
(lcm [5 4 4]) ;=> 20
(lcm []) ;=> 1
(lcm [10]) ;=> 10
