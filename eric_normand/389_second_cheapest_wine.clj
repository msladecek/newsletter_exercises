(require '[clojure.test :refer [deftest is]])


(defn choose-wine [wine-list]
  (let [ordered-by-price (sort-by :price < wine-list)
        chosen-wine (if (>= 2 (count ordered-by-price))
                      (last ordered-by-price)
                      (second ordered-by-price))]
    (:name chosen-wine)))


(deftest test-choose-wine
  (is (= "Second" (choose-wine [{:name "Second" :price 50.00}
                                {:name "First" :price 20.00}
                                {:name "Third" :price 80.00}
                                {:name "Fourth" :price 100.00}])))

  (is (= "White" (choose-wine [{:name "White" :price 10.99}
                               {:name "Red"   :price 8.74}
                               {:name "Rosé"  :price 12.22}])))

  (is (= "White" (choose-wine [{:name "White" :price 10.99}])))

  (is (= "Rosé" (choose-wine [{:name "White" :price 10.99}
                              {:name "Rosé"  :price 12.22}]))))

(test-choose-wine)
