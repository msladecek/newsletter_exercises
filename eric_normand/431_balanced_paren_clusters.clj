(defn clusters [text]
  (loop [open 0
         text text
         paren-clusters []
         open-cluster []]
    (let [ch (first text)
          open-cluster-next (conj open-cluster ch)]
      (cond
        (nil? ch) paren-clusters
        (= \( ch) (recur (inc open)
                         (rest text)
                         paren-clusters
                         open-cluster-next)
        (= 1 open) (recur (dec open)
                          (rest text)
                          (conj paren-clusters (apply str open-cluster-next))
                          [])
        :else (recur (dec open)
                     (rest text)
                     paren-clusters
                     open-cluster-next)))))


(clusters "") ;=> []
(clusters "()") ;=> ["()"]
(clusters "(())") ;=> ["(())"]
(clusters "()()()") ;=> ["()" "()" "()"]
(clusters "(()())()(())") ;=> ["(()())" "()" "(())"]
