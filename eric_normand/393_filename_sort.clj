(require '[clojure.edn :as edn])

(defn filename-order [filename]
  (let [number (-> (re-find #"^(\d+).*$" filename) second edn/read-string)
        lower (clojure.string/lower-case filename)]
    [number lower]))

(sort-by filename-order ["Zoo.txt" "academy.zip"]) ;=> ("academy.zip" "Zoo.txt")
(sort-by filename-order ["12 Final chapter.txt" "2 Second chapter.txt"]) ;=> ("2 Second chapter.txt" "12 Final chapter.txt")
