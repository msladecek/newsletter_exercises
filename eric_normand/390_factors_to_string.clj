

(defn factors->string [numbers]
  (when (seq numbers)
    (let [counts (frequencies numbers)
          factor-strings (map (fn [[factor cnt]]
                                (if (= 1 cnt)
                                  (str factor)
                                  (str factor \^ cnt)))
                              counts)
          total (apply * numbers)]
      (str total " = " (apply str (interpose " x " factor-strings))))))


(factors->string [2 2 2 3])

(factors->string [7])

(factors->string [2 2 7])

(factors->string [1 2 3 0])
