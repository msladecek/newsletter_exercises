(defn split-and-substitute [s]
  (when (seq s)
    (let [digits (re-find #"^\d+" s)]
      (if (seq digits)
        (into [(Integer/toBinaryString (Integer/parseInt digits))]
              (split-and-substitute (subs s (count digits))))
        (let [non-digits (re-find #"^\D+" s)]
          (into [non-digits]
                (split-and-substitute (subs s (count non-digits)))))))))

(defn d->b [s]
  (apply str (split-and-substitute s)))


(d->b "I have 2 arms.") ;=> "I have 10 arms."
(d->b "That costs 3 dollars. But I only have 1 dollar.") ;=> "That costs 11 dollars. But I only have 1 dollar."
(d->b "I was born in 1981.") ;=> "I was born in 11110111101."
