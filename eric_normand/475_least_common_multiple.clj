(defn- lcm2 [num1 num2]
  (let [frac (/ num2 num1)]
    (if-not (ratio? frac)
      (max num1 num2)
      (int (* num2 (denominator frac))))))


(defn lcm [numbers]
  {:pre [(every? pos-int? numbers)]}
  (case (count numbers)
    0 nil
    1 (first numbers)
    (reduce lcm2 numbers)))


(lcm []) ;=> nil (undefined)
(lcm [10]) ;=> 10
(lcm [2 4]) ;=> 4
(lcm [3 7]) ;=> 21
(lcm [2 4 10]) ;=> 20
(lcm [15 2 4]) ;=> 60


(defn lcm-tr [step]
  (let [prev-lcm (volatile! nil)]
    (fn
      ([] (step))
      ([result] (step result))
      ([result input]
       (let [new-lcm (if (nil? @prev-lcm)
                       input
                       (lcm2 input @prev-lcm))]
         (vreset! prev-lcm new-lcm)
         (step result new-lcm))))))


#_(defn lcm [numbers]
  (last (sequence lcm-tr numbers)))
