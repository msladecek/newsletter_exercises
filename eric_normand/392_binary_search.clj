(defn binary-search [value collection]
  (if (> (count collection) 1)
    (let [center (int (/ (count collection) 2))
          center-val (get collection center)]
      (cond
        (= value center-val) true
        (< value center-val) (binary-search value (subvec collection 0 center))
        (> value center-val) (binary-search value (subvec collection center))
        :else false))
    false))


(binary-search 3 [1 2 3]) ;=> true
(binary-search 4 [1 2 5]) ;=> false
(binary-search 10 [1 2 4 5 9 10 11 12]) ;=> true
