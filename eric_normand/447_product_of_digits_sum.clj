(defn num->digits [num]
  (loop [num num
         digits (list)]
    (let [q (quot num 10)
          r (rem num 10)]
      (if (< 0 q)
        (recur q (conj digits r))
        (conj digits r)))))


(defn sum-prod1 [num & nums]
  (->> (iterate (fn [digits]
                  (num->digits (apply * digits)))
                (->> (conj nums num)
                     (apply +)
                     num->digits))
       (drop-while (fn [digits] (<= 2 (count digits))))
       ffirst))


(defn sum-prod2 [num & nums]
  (loop [digits (->> (conj nums num)
                     (apply +)
                     num->digits)]
    (let [prod (apply * digits)]
      (if (< prod 10)
        prod
        (recur (num->digits prod))))))


(sum-prod1 4) ;=> 4
(sum-prod1 10) ;=> 0 (since the sum is two digits, then 1 * 0)
(sum-prod1 11 12) ;=> 6
(sum-prod1 12 16 223) ;=> 0 (work it out!)


(sum-prod2 4) ;=> 4
(sum-prod2 10) ;=> 0 (since the sum is two digits, then 1 * 0)
(sum-prod2 11 12) ;=> 6
(sum-prod2 12 16 223) ;=> 0 (work it out!)
