(require '[clojure.string :as str])


(defn alphabetical? [ch]
  (<= (int \A) (int ch) (int \Z)))


(defn apply-offset [ch offset]
  (-> (int ch)
      (- (int \A))
      (+ offset)
      (mod 26)
      (+ (int \A))
      char))


(defn encode [string]
  (->> string
       str/upper-case
       (reduce (fn [state ch]
                 (if-not (alphabetical? ch)
                   (update state :encoded conj ch)
                   (let [alpha-pos (- (int ch) (int \A))
                         new-ch (apply-offset ch (inc (:prev-offset state)))]
                     (-> state
                         (assoc :prev-offset alpha-pos)
                         (update :encoded conj new-ch)))))
               {:prev-offset -1
                :encoded []})
       :encoded
       (apply str)))


(defn decode [string]
  (->> string
       str/upper-case
       (reduce (fn [state ch]
                 (if-not (alphabetical? ch)
                   (update state :encoded conj ch)
                   (let [new-ch (apply-offset ch (- (inc (:prev-offset state))))
                         alpha-pos (- (int new-ch) (int \A))]
                     (-> state
                         (assoc :prev-offset alpha-pos)
                         (update :encoded conj new-ch)))))
               {:prev-offset -1
                :encoded []})
       :encoded
       (apply str)))


(encode "") ;=> ""
(encode "a") ;=> "A"
(encode "hello") ;=> "HMQXA"
(encode "newsletter") ;=> "NSBPEQYNYW"
(encode "1 hug") ;=> "1 HCB"

(decode "") ;=> ""
(decode "1") ;=> "1"
(decode "HMQXA") ;=> "HELLO"
