(defn pair-match-no-preserve-order [socks]
  (let [counts (frequencies socks)]
    (reduce (fn [counts- [id count-]]
              (let [paired (update counts- :pairs (fn [pairs] (into pairs (repeat (quot count- 2) [id id]))))]
                (if (odd? count-)
                  (update paired :unmatched conj id)
                  paired)))
            {:pairs [] :unmatched []}
            counts)))


(defn pair-match [socks]
  (-> (reduce
       (fn [{:keys [unmatched] :as paired} sock]
         (if (unmatched sock)
           (-> paired
               (update :unmatched disj sock)
               (update :pairs conj [sock sock]))
           (update paired :unmatched conj sock)))
       {:pairs []
        :unmatched #{}}
       socks)
      (update :unmatched vec)))


(pair-match []) ;=> {:pairs [] :unmatched []}
(pair-match [1 2 1]) ;=> {:pairs [[1 1]] :unmatched [2]}
(pair-match [1 2 3 1 2 3 1 1 2]) ;=> {:pairs [[1 1] [2 2] [3 3] [1 1]] :unmatched [2]}
