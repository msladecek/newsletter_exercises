(defn num->digits [num]
  (loop [num num
         digits (list)]
    (let [q (quot num 10)
          r (rem num 10)]
      (if (< 0 q)
        (recur q (conj digits r))
        (conj digits r)))))


(defn count-digits [n]
  (inc (quot n 10)))


(defn superdigit
  ([n]
   (superdigit n 1))
  ([n k]
   (if (and (= 1 (count-digits n))
            (= 1 k))
     n
     (->> (num->digits n)
          (apply +)
          (* k)
          superdigit
          int))))

;           n  k
(superdigit 1  1) ;=> 1
(superdigit 10 2) ;=> 2
(superdigit 11 3) ;=> 6
(superdigit 38 7) ;=> 5
(superdigit 38789789374294723947328946 1000000000000000) ;=> 8
