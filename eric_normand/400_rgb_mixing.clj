(defn hexstr->ints [hexstr]
  (->> (partition 2 (rest hexstr))
       (map #(apply str %))
       (mapv #(BigInteger. % 16))))


(defn ints->hexstr [nums]
  (apply format "#%02X%02X%02X" nums))


(defn mean [& nums]
  (/ (apply + nums)
     (count nums)))


(defn rgb-mix [colors]
  (->> colors
       (map hexstr->ints)
       (apply map mean)
       (map int)
       ints->hexstr))


(rgb-mix ["#FFFFFF" "#000000"]) ;=> "#7F7F7F" or "#808080" depending on how you round
(rgb-mix ["#FF0000" "#00FF00" "#0000FF"]) ;=> "#555555"
