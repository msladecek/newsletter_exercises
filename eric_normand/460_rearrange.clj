(require '[clojure.string :as str])

(defn extract-pos [word]
  (let [[before after] (str/split word #"\d+" 2)
        num-str (re-find #"\d+" word)]
    {:pos (Integer/parseInt num-str)
     :word (str before after)}))

(defn rearrange [string]
  (if (= "" string)
    ""
    (->> (str/split string #" ")
         (map extract-pos)
         (sort-by :pos)
         (map :word)
         (interpose " ")
         (apply str))))

(rearrange "World2! He1llo,") ;=> "Hello, World!"
(rearrange "fo3r 5more Elegan1t 2weapons age.7 civil6ized a4") ;=> "Elegant weapons for a more civilized age."
(rearrange "") ;=> ""
