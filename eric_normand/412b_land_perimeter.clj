(defn enumerate [coll]
  (map vector (range) coll))

(defn neighbour-positions [[rownum colnum]]
  #{[(inc rownum) colnum]
    [(dec rownum) colnum]
    [rownum (inc colnum)]
    [rownum (dec colnum)]})

(defn perimeter [ocean]
  (let [land-positions
        (into #{} (for [[rownum row] (enumerate ocean)
                        [colnum cell] (enumerate row)
                        :when (= 1 cell)]
                    [rownum colnum]))]
    (->> land-positions
         (mapcat neighbour-positions)
         (map #(if (land-positions %) 0 1))
         (reduce +))))


(perimeter [[1]]) ;=> 4

;; Likewise, a single square filled with water has a perimeter of 0:
(perimeter [[0]]) ;=> 0

;; Two squares of land next to each other share an edge, which reduces the perimeter:
(perimeter [[1 1]]) ;=> 6

;; The edge of the grid is like an implicit encircling of water:
(perimeter [[1 1]
            [1 1]]) ;=> 8

(perimeter [[0 0 0 0]
            [0 1 1 0]
            [0 1 1 0]
            [0 0 0 0]]) ;=> 8 (same!)

;; Here are some other weird shapes:

(perimeter [[1 1 1 1 1 1]
            [1 0 0 0 0 1]
            [1 0 1 1 0 1]
            [1 0 0 0 0 1]
            [1 1 1 1 1 1]]) ;=> 42

(perimeter [[0 1 0 0]
            [1 1 1 0]
            [0 1 0 0]
            [1 1 0 0]]) ;=> 16
