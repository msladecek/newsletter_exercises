(defn sort-by-content [nums]
  (vec
   (sort-by
    identity
    (fn [a b]
      (cond
        (and (coll? a) (coll? b))
        (first (filter (complement zero?) (map compare a b)))

        (coll? a)
        (compare (first a) b)

        (coll? b)
        (dec (compare a (first b)))

        :else
        (compare a b)))
    nums)))

(sort-by-content [4 5 3 2 1]) ;=> [1 2 3 4 5]

;; we sort sequences by their first element, then by second element, then by third, etc
(sort-by-content [[2 3] [0 9] [-1 3 4] [0 3]]) ;=> [[-1 3 4] [0 3] [0 9] [2 3]]

;; we sort numbers and sequences together as if numbers were sequences of length 1
(sort-by-content [5 [4 5] 3 1 [0 2 3]]) ;=> [[0 2 3] 1 3 [4 5] 5]

;; but numbers sort before sequences when the number is the same as the first element
(sort-by-content [[1] 1]) ;=> [1 [1]]
