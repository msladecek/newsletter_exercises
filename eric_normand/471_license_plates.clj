(defn regroup [plate group-size]
  {:pre [(pos-int? group-size)]}
  (->> (filter #(not= \- %) plate)
       reverse
       (partition-all group-size)
       (map reverse)
       reverse
       (map #(apply str %))
       (interpose \-)
       (apply str)))


(regroup "A5-GG-B88" 3) ;=> "A-5GG-B88"
(regroup "A5-GG-B88" 2) ;=> "A-5G-GB-88"
(regroup "6776" 2) ;=> "67-76"
(regroup "F33" 1) ;=> "F-3-3"
(regroup "IIO" 7) ;=> "IIO"
(regroup "IIO" 0) ;=> assertion error
