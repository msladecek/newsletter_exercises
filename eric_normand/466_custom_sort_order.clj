(defn sort-with [ordering coll]
  (let [keyfn (into {} (map vector ordering (range)))]
    (sort-by keyfn coll)))


(sort-with [:breakfast :lunch :dinner] #{:lunch :breakfast :dinner})
;=> (:breakfast :lunch :dinner)


(sort-with [2 3 4 :jack :queen :king :ace] [4 2 4 :king 2])
;=> [2 2 4 4 :king]
