(defn collatz [num]
  (if (= 1 num)
    (list 1)
    (let [cont (if (odd? num)
                 (inc (* 3 num))
                 (/ num 2))]
      (lazy-seq (cons num (collatz cont))))))


(collatz 1) ;=> (1)
(collatz 2) ;=> (2 1)
(collatz 3) ;=> (3 10 5 16 8 4 2 1)
