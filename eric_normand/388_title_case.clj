(require '[clojure.test :refer [is]]
         '[clojure.string :as str])

(def minor-word? #{"and" "but" "if" "nor" "or" "so" "yet" "a" "an" "the" "as" "at" "by" "for" "in" "of" "off" "on" "per" "to" "up" "via"})

(defn title-case [string]
  (let [words (str/split string #" ")
        first-word (first words)
        title-cased-words (into [(str/capitalize first-word)]
                                (map (fn [word]
                                       (if (minor-word? word)
                                         word
                                         (str/capitalize word)))
                                     (rest words)))]
    (apply str (interpose " " title-cased-words))))


(title-case "the hobbit")


(and
  (is (= (title-case "the hobbit") "The Hobbit"))
  (is (= (title-case "the fellowship of the ring")  "The Fellowship of the Ring"))
  (is (= (title-case "the two towers")  "The Two Towers"))
  (is (= (title-case "the return of the king")  "The Return of the King")))
