(require '[clojure.test :refer [is]]
         '[clojure.string :as str])

(defn clean [text banned-words]
  (reduce (fn [text word]
            (let [pattern (re-pattern (str "(?i)\\Q" word "\\E"))
                  replacement (apply str (repeat (count word) \*))]
              (str/replace text pattern replacement)))
          text
          banned-words))

(is
 (= (clean "You are a farty pants." ["fart" "poop"])
    "You are a ****y pants."))

(is
 (= (clean "Curse this site!" ["jinx" "curse"])
    "***** this site!"))
