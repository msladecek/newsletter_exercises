(defn equiv? [scheme1 scheme2]
  (let [mapping (zipmap scheme1 scheme2)]
    (when (= (seq scheme2) (map mapping scheme1))
      (->> mapping
           (remove #(= (key %) (val %)))
           (into {})))))


(equiv? "ABCB" "XALA") ;=> {\A \X \B \A \C \L}
(equiv? "A" "A") ;=> {}
(equiv? "A" "B") ;=> {\A \B}
(equiv? "AB" "A") ;=> nil
(equiv? "ABB" "XXY") ;=> nil
