(require '[clojure.string :as str])


(defn phrases [maxlen base]
  (let [words (str/split base #" ")
        completed-phrases
        (loop [wip-phrases []
               words words]
          (cond
            (empty? words)
            wip-phrases

            (empty? wip-phrases)
            (recur (conj wip-phrases [])
                   words)

            :else
            (let [word (first words)
                  extended-phrase (->> (conj (last wip-phrases) word)
                                       (interpose " ")
                                       (apply str)
                                       count)]
              (cond
                (<= extended-phrase maxlen)
                (recur (conj (subvec wip-phrases 0 (dec (count wip-phrases))) (conj (last wip-phrases) word))
                       (rest words))

                (<= (count word) maxlen)
                (recur (conj wip-phrases [word])
                       (rest words))

                :else
                (recur wip-phrases
                       (rest words))))))]
    (->> completed-phrases
         (remove empty?)
         (mapv (fn [words] (apply str (interpose " " words)))))))


(phrases 10 "she sells sea shells by the sea shore")   ;=> ["she sells" "sea shells" "by the sea" "shore"]
(phrases 2 "she sells sea shells by the sea shore")    ;=> ["by"]
(phrases 2 "the big dog jumped over the fence")        ;=> []
(phrases 13 "she sells sea shells by the sea shore")   ;=> ["she sells sea" "shells by the" "sea shore"]
