(require '[clojure.test :refer [is deftest]])


(defn parallel? [[a1 b1 c1] [a2 b2 c2]]
  (cond
    (and (zero? b1) (zero? b2)) (== (/ c1 a1) (/ c2 a2))
    (or (zero? b1) (zero? b2)) false
    (== (/ a1 b1) (/ a2 b2)) (not (== (/ c1 b1) (/ c2 b2)))))


(deftest test-parallel
  (is (parallel? [2 4 1] [1 2 1]))
  (is (not (parallel? [0 1 2] [1 0 2])))
  (is (not (parallel? [2 4 1] [4 2 1])))
  (is (not (parallel? [0 1 5] [0 1 5]))))


(do
  (println "---")
  (test-parallel))
