(defn partitions [items]
  (for [started (take-while seq (iterate rest items))
        ended (take-while seq (iterate butlast started))]
    ended))


(defn number->digits [number]
  (loop [number number
         digits (list)]
    (let [q (quot number 10)
          r (rem number 10)]
      (if (< 0 q)
        (recur q (conj digits r))
        (conj digits r)))))


(defn digits->number [digits]
  (reduce (fn [sum digit] (+ (* 10 sum) digit)) digits))


(defn prime? [number]
  (case number
    1 false
    2 true
    (->> (range 2 (inc (Math/sqrt number)))
         (map #(mod number %))
         (some zero?)
         not)))


(defn find-primes [number]
  (let [digits (number->digits number)]
    (->> digits
         partitions
         (filter seq)
         (map digits->number)
         (filter prime?)
         sort
         (into []))))


(find-primes 2) ;=> [2]
(find-primes 22) ;=> [2 2]
(find-primes 717) ;=> [7 7 17 71]
(find-primes 1) ;=> []
(find-primes 44) ;=> []
