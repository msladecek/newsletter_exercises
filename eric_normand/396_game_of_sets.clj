(defn game-set? [cards]
  (let [aggregated (apply merge-with #(conj (if (vector? %1) %1 [%1]) %2) cards)]
    (every? #(or (apply distinct? %)
                 (apply = %))
            (vals aggregated))))


(game-set?
 [{:color :purple :number 3 :shape :diamond :shading :full}
  {:color :red    :number 3 :shape :diamond :shading :lines}
  {:color :green  :number 3 :shape :diamond :shading :empty}]) ; => true

(game-set?
 [{:color :purple :number 3 :shape :diamond :shading :full}
  {:color :red    :number 3 :shape :diamond :shading :lines}
  {:color :purple :number 3 :shape :diamond :shading :empty}]) ; => false
