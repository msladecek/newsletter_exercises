(defn spots [numbers candidate]
  (let [happy-pred (if (odd? candidate) odd? even?)]
    (->> (map vector (range) (partition 2 1 numbers))
         (map (fn [[left-id neighbours]]
                (let [middle-id (+ left-id 0.5)]
                  (if (some happy-pred neighbours)
                    {:happy [middle-id]}
                    {:unhappy [middle-id]}))))
         (apply merge-with into))))


(spots [1 1]     1) ;=> {:happy [0.5]                   }
(spots [1 1]     2) ;=> {                 :unhappy [0.5]}
(spots [1 1 2]   4) ;=> {:happy [1.5]     :unhappy [0.5]}
(spots [1 1 2 2] 3) ;=> {:happy [0.5 1.5] :unhappy [2.5]}
