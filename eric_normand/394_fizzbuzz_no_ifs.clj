(defn fizzbuzz []
  (map
   (fn [num fizz buzz]
     ({[true false] "Fizz"
       [false true] "Buzz"
       [true true] "FizzBuzz"
       [false false] num}
      [fizz buzz]))
   (range) (cycle [true false false]) (cycle [true false false false false])))

(doseq [val (take 100 (fizzbuzz))]
  (println val))
