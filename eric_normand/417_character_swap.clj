(defn overlapping-patterns [pat-a pat-b]
  (let [one-side (fn [pat-a pat-b]
                   (let [subpats-b (for [len (range 1 (inc (count pat-b)))]
                                     [(subs pat-b 0 len) (subs pat-b len)])]
                     (for [[pre post] subpats-b
                           :when (.endsWith pat-a pre)]
                       (str pat-a post))))]
    (into #{}
          (concat
           (if (.contains pat-a pat-b) [pat-a] [])
           (if (.contains pat-b pat-a) [pat-b] [])
           (one-side pat-b pat-a)
           (one-side pat-a pat-b)))))


(defn swap [main-str substr-a substr-b]
  (when-not (some #(.contains main-str %) (overlapping-patterns substr-a substr-b))
    (let [augmented-main-str
          (str main-str " ")

          augmented-replaced
          (->> (clojure.string/split augmented-main-str (re-pattern substr-a))
               (map (fn [component-a]
                      (let [components-b (clojure.string/split component-a (re-pattern substr-b))]
                        (apply str (interpose substr-a components-b)))))
               (interpose substr-b)
               (apply str))]
      (subs augmented-replaced 0 (count main-str)))))


(swap "abc" "a" "b") ;=> "bac"
(swap "book" "k" "t") ;=> "boot"
(swap "book" "t" "k") ;=> "boot"
(swap "Closure" "j" "s") ;=> "Clojure"
(swap "bee" "b" "e") ;=> "ebb"
(swap "abc" "1" "2") ;=> "abc"
(swap "hello" "el" "lo") ;=> "hloel"

(swap "helo" "ell" "llo") ;=> "helo"
(swap "hello" "ell" "llo") ;=> ?
(swap "helllo" "ell" "llo") ;=> ?
(swap "hellllo" "ell" "llo") ;=> "hlloell"
