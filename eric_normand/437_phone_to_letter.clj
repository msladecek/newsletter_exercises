(require '[clojure.math.combinatorics :as combo])


(def key-mapping
  {\1 #{}
   \2 #{\a \b \c}
   \3 #{\d \e \f}
   \4 #{\g \h \i}
   \5 #{\j \k \l}
   \6 #{\m \n \o}
   \7 #{\p \q \r \s}
   \8 #{\t \u \v}
   \9 #{\w \x \y \z}
   \0 #{\space}})


(defn digits->letters [string]
  (->> string
       (map #(get key-mapping % #{%}))
       (apply combo/cartesian-product)
       (mapv #(apply str %))))


(digits->letters "22") ;=> ["aa" "ab" "ac" "ba" "bb" "bc" "ca" "cb" "cc"]
(digits->letters "q2") ;=> ["qa" "qb" "qc"]
