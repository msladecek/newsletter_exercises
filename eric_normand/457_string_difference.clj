(defn diff [str-a str-b]
  (let [counts-a (frequencies str-a)
        counts-b (frequencies str-b)
        counts-diff (merge-with - counts-b counts-a)]
    (if-not (= [1] (remove zero? (vals counts-diff)))
      (throw (ex-info "arguments must differ in exactly one character" {}))
      (key (first (filter #(= 1 (val %)) counts-diff))))))


(diff "" "j") ;=> \j
(diff "a" "ab") ;=> \b
(diff "abc" "xcab") ;=> \x
(diff "xxyyzz" "xzyfyxz") ;=> \f
(diff "cccvv" "cvvcvc") ;=> \v
