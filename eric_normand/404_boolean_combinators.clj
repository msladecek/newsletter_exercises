(def norman
  {:name "norman"
   :married true
   :contact {:address {:country "norway"
                       :city "oslo"}}
   :pets [{:name "erik"
           :type "dog"}]})

(def martin
  {:name "martin"
   :married false
   :contact {:email "contact@martin.martin"
             :address {:country "france"
                       :city "biarritz"}}
   :pets [{:name "elbo"
           :type "cat"}
          {:name "corgonzola"
           :type "dog"}]})

(def floki
  {:name "floki"
   :married true
   :contact {:address {:country "iceland"
                       :city "reykiavik"}}
   :pets [{:name "loki"
           :type "dog"}]})


(defn has-dog? [person]
  (boolean (some #{"dog"} (map :type (:pets person)))))

(defn lives-in-iceland? [person]
  (= "iceland" (get-in person [:contact :address :country])))

(defn married? [person]
   (:married person))

(defn rule-and
  ([] (fn [_person] true))
  ([rule] (fn [person] (rule person)))
  ([rule1 rule2] (fn [person] (and (rule1 person) (rule2 person))))
  ([rule1 rule2 & rules] (reduce rule-and (rule-and rule1 rule2) rules)))


(defn rule-or
  ([] (fn [_person] false))
  ([rule] (fn [person] (rule person)))
  ([rule1 rule2] (fn [person] (or (rule1 person) (rule2 person))))
  ([rule1 rule2 & rules] (reduce rule-or (rule-or rule1 rule2) rules)))


(defn rule-not [rule]
  (fn [person]
    (not (rule person))))


(def married-icelander? (rule-and married? lives-in-iceland?))
(def unmarried-dog-owner (rule-and (rule-not married?) has-dog?))
(def married-icelander-without-dog? (rule-and married? lives-in-iceland? (rule-not has-dog?)))

(married-icelander floki)
(married-icelander-without-dog? floki)
