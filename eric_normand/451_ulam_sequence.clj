(require '[clojure.math.combinatorics :as combo]
         '[clojure.set :as set])


(defn ulam-next [previous]
  (->> (combo/combinations previous 2)
       (map #(apply + %))
       (remove previous)
       frequencies
       (filter (fn [[value freq]] (= freq 1)))
       (map (fn [[value freq]] value))
       (apply min)
       (conj previous)))


(defn ulam []
  (->> (sorted-set 1 2)
       (iterate ulam-next)
       (map last)
       (cons 1)))


(take 2 (ulam)) ;=> (1 2)
(take 5 (ulam)) ;=> (1 2 3 4 6)
(take 17 (ulam)) ;=> (1 2 3 4 6 8 11 13 16 18 26 28 36 38 47 48 53)
