(defn identify [digit]
  (cond
    (odd? digit) :odd
    (even? digit) :even
    :else nil))

(defn char->int [ch]
  (- (int ch) (int \0)))

(defn longest-alt-subs [string]
  (loop [substrings #{""}
         digits (map char->int string)
         current-pos 0
         prev-cls nil
         current-start 0]
    (if (not (seq digits))
      (apply max-key count (conj substrings (subs string current-start current-pos)))
      (let [cls (identify (first digits))]
        (if (= cls prev-cls)
          (recur (conj substrings (subs string current-start current-pos))
                 (rest digits)
                 (inc current-pos)
                 cls
                 current-pos)
          (recur substrings
                 (rest digits)
                 (inc current-pos)
                 cls
                 current-start))))))


(longest-alt-subs "") ;=> ""
(longest-alt-subs "1") ;=> "1"
(longest-alt-subs "123") ;=> "123"
(longest-alt-subs "13") ;=> "1"
(longest-alt-subs "122381") ;=> "2381"
