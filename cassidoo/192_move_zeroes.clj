(defn move-zeroes-transducer [step]
  (let [zero-count (volatile! 0)]
   (fn
     ([]
      (step))
     ([result]
       (reduce step result (repeat @zero-count 0)))
     ([result input]
      (if (zero? input)
        (do (vswap! zero-count inc)
            result)
        (step result input))))))


(defn move-zeroes [coll]
  (into [] move-zeroes-transducer coll))


(move-zeroes [0 2 0 3 8]) ;=> [2 3 8 0 0]
