(require '[clojure.math.combinatorics :as combo]
         '[clojure.set :as set])

(defn line-params
  "Line in the form (y1 - y2) * x + (x2 - x1) * y + (x1 * y2 - x2 * y1) = 0"
  [[x1 y1] [x2 y2]]
  (let [not-zero? (complement zero?)
        a (- y1 y2)
        b (- x2 x1)
        c (- (* x1 y2) (* x2 y1))
        params [a b c]]
    (cond
      (not-zero? a) (mapv #(/ % a) params)
      (not-zero? b) (mapv #(/ % b) params)
      (not-zero? c) (mapv #(/ % c) params)
      :else params)))

(defn max-points-on-line
  "Given n points on a 2D plane, find the maximum number of points that lie on the same straight line."
  [points]
  (let [pairs (combo/combinations points 2)
        lines (for [[pt1 pt2] pairs]
                {(line-params pt1 pt2) #{pt1 pt2}})
        grouped-lines (apply merge-with set/union lines)]
    (->> (vals grouped-lines)
         (map count)
         (apply max))))

(max-points-on-line [[1,1],[3,2],[5,3],[4,1],[2,3],[1,4]])
