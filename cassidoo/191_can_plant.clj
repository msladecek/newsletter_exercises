(defn can-plant [garden plant-count]
  (let [augmented-garden
        (-> garden
            (#(if (= 1 (first %)) (into [0] %) %))
            (#(if (= 1 (last %)) (conj % 0) %)))]
    (->> (partition-by identity augmented-garden)
         (filter #(= 0 (first %)))
         (map count)
         (map #(if (odd? %) (quot % 2) (dec (quot % 2))))
         (reduce +)
         (<= plant-count))))


(can-plant [1 0 0 1] 1) ;=> false
(can-plant [1 0 0 0 1] 1) ;=> true
(can-plant [1 0 0 0 1] 4) ;=> false
(can-plant [1 0 0 0 0 1] 2) ;=> false
(can-plant [1 0 0 0 0 0 1] 2) ;=> true
