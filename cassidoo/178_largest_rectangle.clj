(defn expansion-down [rectangle]
  (let [max-row (apply max (map first rectangle))]
    (->> rectangle
         (filter (fn [[r _c]] (= max-row r)))
         (map (fn [[r c]] [(inc r) c])))))


(defn expansion-right [rectangle]
  (let [max-col (apply max (map second rectangle))]
    (->> rectangle
         (filter (fn [[_r c]] (= max-col c)))
         (map (fn [[r c]] [r (inc c)])))))


(defn expansion-downright [rectangle]
  (let [max-row (apply max (map first rectangle))
        max-col (apply max (map second rectangle))]
    (into [[(inc max-row) (inc max-col)]]
          (concat (expansion-right rectangle)
                  (expansion-down rectangle)))))


(defn largest-rectangle [matrix]
  (let [max-row (count matrix)
        max-col (count (first matrix))
        ones (into #{}
                   (for [r (range max-row)
                         c (range max-col)
                         :when (= 1 (get-in matrix [r c]))]
                     [r c]))]
    (loop [max-size 0
           rectangles (set (map vector ones))]
      (let [next-rectangles
            (into #{}
                  (for [rect rectangles
                        expansion [(expansion-down rect)
                                   (expansion-right rect)
                                   (expansion-downright rect)]
                        :when (every? ones expansion)]
                    (into rect expansion)))]
        (if-not (seq next-rectangles)
          max-size
          (recur (reduce max max-size (map count next-rectangles))
                 next-rectangles))))))


(largest-rectangle
 [[1 0 1 0 0]
  [1 0 1 1 1]
  [1 1 0 1 1]
  [1 0 0 1 0]]) ; => 4


(largest-rectangle
 [[1 0 1 0 0]
  [1 0 1 1 1]
  [1 1 1 1 1]
  [1 0 0 1 0]]) ; => 6


(largest-rectangle
 [[1 0 1 0 0]
  [1 0 1 1 1]
  [1 1 1 1 1]
  [1 0 0 1 0]
  [1 0 0 0 0]
  [1 0 0 0 0]
  [1 0 0 0 0]]) ; => 7
