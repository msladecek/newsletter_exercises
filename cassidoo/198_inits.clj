(defn inits [items]
  (reductions conj [] items))


(inits [4 3 2 1]) ;=> ([] [4] [4 3] [4 3 2] [4 3 2 1])
(inits [144]) ;=> ([] [144])
