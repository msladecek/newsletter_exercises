(defn count-binary-ones [num]
  (loop [num num cnt 0]
    (cond
      (zero? num) cnt
      (zero? (mod num 2)) (recur (quot num 2) cnt)
      :else (recur (quot num 2) (inc cnt)))))
