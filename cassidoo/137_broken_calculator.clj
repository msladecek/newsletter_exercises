(defn broken-calc [have want]
  (loop [want want, op-count 0]
    (cond
      (= have want) op-count
      (> have want) (- have want)
      (odd? want) (recur (inc want) (inc op-count))
      :else (recur (/ want 2) (inc op-count)))))

(do
  (println)
  (for [[x y op-count] [[3 10 3] [10 10 0] [1 2 1] [1 3 3] [10 3 7]]]
    (let [expr `(~'broken-calc ~x ~y)
          result (eval expr)]
      (if (= op-count result)
        (println "  correct:" (str expr) "=" result)
        (println "incorrect:" (str expr) "=" result ", should be" op-count)))))
