(defn zeros-ending-factorial [num]
  (->> (iterate #(* 5 %) 5)
       (take-while #(<= % num))
       (map #(quot num %))
       (reduce +)))


(zeros-ending-factorial 1) ;=> 0
(zeros-ending-factorial 5) ;=> 1
(zeros-ending-factorial 100) ;=> 24
