(defn distinct-count [items]
  (count (set items)))

(defn char-num-sort [words]
  (sort-by (juxt distinct-count (comp - count)) words))

(char-num-sort ["Bananas" "do" "not" "grow" "in" "Mississippi"])
;; => ("do" "in" "not" "Mississippi" "Bananas" "grow")
