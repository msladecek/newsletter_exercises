(comment "Given two numbers X and Y, write a function to check that X and Y are in golden ratio.")

(defn golden-ratio? [x y]
  (let [[larger smaller] (sort [x y])]
    (= (/ larger smaller)
       (/ (+ larger smaller) larger))))

(golden-ratio? )
