;; Your music library contains N songs
;; and your friend wants to listen to L songs during your road trip (repeats are allowed).
;; Make a playlist so that every song is played at least once,
;; and a song can only be played again only if K other songs have been played.
;; Return the number of possible playlists.

(require '[clojure.math.combinatorics :as combo]
         '[clojure.set :as set])


(defn not-repeats [playlist step]
  (let [group-size (inc step)]
    (every? (fn [group]
              (= group-size (count (set group))))
            (partition group-size 1 playlist))))


(defn playlists [library playlist-length before-repeat]
  (let [library (set library)
        combos (combo/selections library playlist-length)]
    (if (< (count library) playlist-length)
      (list)
      (->> combos
           (filter #(set/subset? library (set %)))
           (filter #(not-repeats % before-repeat))))))


(defn num-playlists [n l k]
  ;; This is a pretty shitty solution because the `playlists` function has exponential performance.
  ;; I suspect that result could be calculated in O(1) instead.
  (let [library (range l)]
    (count (playlists library n k))))


(num-playlists 3 3 1) ; => 6
