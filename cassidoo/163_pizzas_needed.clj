(defn pizzas-needed [requests slices-per-pizza]
  (let [total-slices-requested (reduce + (map :num requests))]
    (loop [pizza-count 1]
      (if (< (* pizza-count slices-per-pizza) total-slices-requested)
        (recur (inc pizza-count))
        pizza-count))))

(def requests
  [{:name "Joe" :num 9}
   {:name "Cami" :num 3}
   {:name "Cassidy" :num 4}])

(pizzas-needed requests 8)
