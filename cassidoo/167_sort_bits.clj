(defn count-binary-ones [num]
  (loop [num num cnt 0]
    (cond
      (zero? num) cnt
      (zero? (mod num 2)) (recur (quot num 2) cnt)
      :else (recur (quot num 2) (inc cnt)))))


(defn sort-bits [nums]
  ;; `count-binary-ones` from newsletter #147
  (sort-by count-binary-ones nums))


(sort-bits [0,1,2,4,8,3,5,6,7]) ;; => [0,1,2,4,8,3,5,6,7]
