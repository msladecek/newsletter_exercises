(defn valid-tick-tack-toe [board]
  (let [symbol-counts
        (frequencies (apply concat board))

        winning-triplets
        (->> (concat (mapv vec board)
                     (apply mapv vector board)
                     [[(get-in board [0 0]) (get-in board [1 1]) (get-in board [2 2])]
                      [(get-in board [2 0]) (get-in board [1 1]) (get-in board [0 2])]])
             (filter (complement #{[\space \space \space]}))
             (filter #(apply = %)))]
    (boolean
     (and (#{0 1} (- (get symbol-counts \X 0)
                     (get symbol-counts \O 0)))
          (#{0 1} (count winning-triplets))))))


(valid-tick-tack-toe ["XOX", " X ", "   "]) ;=> false
(valid-tick-tack-toe ["XOX", "O O", "X X"]) ;=> true
(valid-tick-tack-toe ["OOO", "   ", "XXX"]) ;=> false
(valid-tick-tack-toe ["  O", "   ", "   "]) ;=> false
