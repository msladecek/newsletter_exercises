(defprotocol ProductList
  (add [this item])
  (product [this item-count]))


(defn new-product-list []
  (let [items (atom ())]
    (reify
      ProductList
      (add [this item] (swap! items conj item))
      (product [this item-count] (reduce * (take item-count @items))))))


(let [product-list (new-product-list)]
  (add product-list 7)
  (add product-list 0)
  (add product-list 2)
  (add product-list 5)
  (add product-list 4)
  (product product-list 3)) ; => 40
