(defn num->digits [num]
  (loop [num num
         digits (list)]
    (let [q (quot num 10)
          r (rem num 10)]
      (if (< 0 q)
        (recur q (conj digits r))
        (conj digits r)))))


(defn same-digits [num]
  (= (into #{} (num->digits num))
     (into #{} (num->digits (* num num num)))))


(same-digits 1) ;=> true
(same-digits 10) ;=> true
(same-digits 251894) ;=> true
(same-digits 251895) ;=> false
