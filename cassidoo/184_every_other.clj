(defn every-other [s]
  (->> s
       (partition-by identity)
       (map (comp dec count))
       (reduce +)))


(every-other "xxyxxy") ; => 2
(every-other "yyyyy") ; => 4
