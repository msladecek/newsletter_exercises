(require '[clojure.pprint :refer [pprint]]
         '[clojure.set :as set])


(defn fibonacci-like [inp-seq]
  (let [first-pair (subvec inp-seq 0 2)
        valid-seqs
        (loop [seqs {(reduce + first-pair) #{first-pair}}
               inp-so-far first-pair
               inp-seq (drop 2 inp-seq)]
          (let [num (first inp-seq)
                matching-seqs (seqs num)]
            (if-not num
              seqs
              (if matching-seqs
                (recur (merge-with set/union seqs (into {} (map (fn [s] [(+ num (last s)) #{(conj s num)}]) matching-seqs)))
                       (conj inp-so-far num)
                       (rest inp-seq))
                (recur (merge-with set/union seqs (into {} (for [m inp-so-far] [(+ m num) #{[m num]}])))
                       (conj inp-so-far num)
                       (rest inp-seq))))))
        greatest-length (->> (vals valid-seqs)
                             (mapcat set/union)
                             (map count)
                             (apply max))]
    greatest-length))


(fibonacci-like [1,3,7,11,12,14,18])
