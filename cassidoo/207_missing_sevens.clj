(require '[clojure.math.combinatorics :as combo])


(defn compnents [grid]
  (concat grid (apply map vector grid)))


(defn solved? [grid]
  (->> (compnents grid)
       (map #(apply + %))
       (apply =)))


(defn gridpos [i]
  [(quot i 3) (rem i 3)])


(defn missing-sevens [grid]
  (->> (combo/subsets (range 9))
       (map (fn [seq-i]
              (if (empty? seq-i)
                grid
                (reduce
                 (fn [grid2 pos] (assoc-in grid2 (gridpos pos) 7))
                 grid
                 seq-i))))
       (filter solved?)
       first
       (apply concat)
       (filter #(= 7 %))
       count))


(missing-sevens [[9,4,3],[3,4,9],[4,8,4]]) ; => 0
(missing-sevens [[1,5,2],[5,9,5],[6,5,3]]) ; => 4
(missing-sevens [[3,9,6],[8,5,5],[8,4,0]]) ; => 2
