(defn close-enough? [node1 node2]
  (let [diff (map - node1 node2)
        diff-len-squared (apply + (map #(* % %) diff))]
    (>= 25 diff-len-squared)))


(defn can-toggle [{:keys [switch hub light]}]
  (loop [unvisited (set hub)
         layer #{switch}]
    (if (some #(close-enough? % light) layer)
      true
      (let [visitable
            (into #{}
                  (filter (fn [node]
                            (some #(close-enough? % node)
                                  layer))
                          unvisited))]
        (if-not (seq visitable)
          false
          (recur (clojure.set/difference unvisited visitable)
                 visitable))))))


(can-toggle {:switch [0 1]
             :hub [[2 1] [2 5]]
             :light [1 6]}) ; => true

(can-toggle {:switch [0 0]
             :hub [[0 5] [0 10] [0 15]]
             :light [0 20]}) ; => true

(can-toggle {:switch [0 0]
             :hub [[0 5] [0 15]]
             :light [0 20]}) ; => false
