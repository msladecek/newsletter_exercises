(require '[criterium.core :as cc])


(defn stock-buy-sell [prices]
  (let [daily-prices (map-indexed
                      (fn [day price]
                        {:day (inc day) :price price})
                      prices)
        running-min (->> daily-prices
                         (reductions #(min-key :price %1 %2)))
        running-max (->> daily-prices
                         reverse
                         (reductions #(max-key :price %1 %2))
                         reverse)
        daily-diff (map
                    (fn [min-sofar max-sofar]
                      {:diff (- (:price max-sofar)
                                (:price min-sofar))
                       :day-min (:day min-sofar)
                       :day-max (:day max-sofar)})
                    running-min running-max)]
    (apply max-key :diff daily-diff)))


(defn print-max-diff [diff]
  (str "Buy on day " (:day-min diff) ", sell on day " (:day-max diff) "."))


(def stock-prices [110, 180, 260, 40, 310, 535, 695])
  
(-> (stock-buy-sell stock-prices)
    print-max-diff)

(cc/quick-bench
 (stock-buy-sell stock-prices))
