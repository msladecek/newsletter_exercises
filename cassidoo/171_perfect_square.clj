(defn half [num] (quot num 2))
(defn square [num] (* num num))


(defn perfect-square [num]
  (loop [low 0, high (dec num)]
    (let [mid (half (+ low high))
          mid-squared (square mid)]
      (cond
        (#{0 1} num) true
        (< high low) false
        (< mid-squared num) (recur (inc mid) high)
        (< num mid-squared) (recur low (dec mid))
        :else true))))


(perfect-square 25) ;=> true
(perfect-square 1) ;=> true
(perfect-square 2) ;=> false
(perfect-square 4) ;=> true
(perfect-square 20) ;=> false
(perfect-square 48) ;=> false
(perfect-square 49) ;=> true
