(require '[clojure.string :as str])

(defn if-backspace-then-pop-else-conj [characters ch]
  (if (= \# ch)
    (if (seq characters)
      (pop characters)
      characters)
    (conj characters ch)))

(defn resolve-backspace [str1]
  (apply str (reduce if-backspace-then-pop-else-conj [] str1)))

(defn compare-with-backspace [& strings]
  (apply = (map resolve-backspace strings)))

(for [[str1 str2] [["a##c" "#a#c"] ["xy##" "z#w#"] ["ab##" "##ab"]]]
  (let [expr `(~'compare-with-backspace ~str1 ~str2)]
    (str expr " -> " (eval expr))))
