(defn num-chars [str char]
  (reduce
   (fn [sum ch] (if (= ch char) (inc sum) sum))
   0
   str))


(num-chars "oh heavens" \h)
