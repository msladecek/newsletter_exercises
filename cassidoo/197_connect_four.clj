(def board-size {:width 7 :height 6})


(defn init []
  (into [] (repeatedly (:width board-size) vector)))


(defn move [board column color]
  (cond
    (not (<= 0 column (:width board-size)))
    :column-out-of-range

    (<= (:height board-size) (count (board column)))
    :column-already-full

    :else
    (update board column conj color)))



(defn enumerate [s]
  (map vector (range) s))


(defn winning-subsets []
  (let [horizontal (for [row (range (:height board-size))
                         col-start (range (- (:width board-size) 4))
                         :let [col-end (+ 4 col-start)]]
                     (for [col (range col-start col-end)]
                       [col row]))
        vertical (for [col (range (:width board-size))
                       row-start (range (- (:height board-size) 4))
                       :let [row-end (+ 4 row-start)]]
                   (for [row (range row-start row-end)]
                     [col row]))
        diagonal-upward (for [row (range (- (:height board-size) 4))
                              col (range (- (:width board-size) 4))]
                              (for [offset (range 4)]
                          [(+ col offset) (+ row offset)]))
        diagonal-downward (for [row (range 4 (:height board-size))
                                col (range (- (:width board-size) 4))]
                              (for [offset (range 4)]
                            [(+ col offset) (- row offset)]))]
    (into #{}
          (for [subset (concat horizontal vertical diagonal-upward diagonal-downward)]
            (into #{} subset)))))



(defn winner [board]
  (let [claimed-places (->> (group-by
                             first
                             (for [[col-id column] (enumerate board)
                                   [row-id color] (enumerate column)]
                               [color [col-id row-id]]))
                            (map (fn [[color annotated-places]]
                                   [color (into #{} (map second annotated-places))]))
                            (into {}))
        possible-winning-places (winning-subsets)]
    (->> claimed-places
         (filter (fn [[_color places]]
                   (some #(clojure.set/subset? % places) possible-winning-places)))
         (map first)
         first)))


(comment
  (-> (init)
      (move 0 :yellow)
      (move 1 :red)
      (move 1 :yellow)
      (move 2 :red)
      (move 3 :yellow)
      (move 2 :red)
      (move 2 :yellow)
      (move 3 :red)
      (move 3 :yellow)
      (move 4 :red)
      (move 3 :yellow)
      winner
      )

  )
