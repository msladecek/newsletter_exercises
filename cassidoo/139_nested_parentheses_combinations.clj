(require '[clojure.string :as str])

(defn generate-parens
  ([n]
   (generate-parens n 0 ""))
  ([open-budget close-debt prefix]
   (if (and (zero? open-budget)
            (zero? close-debt))
     [prefix]
     (lazy-cat
      (when (pos? open-budget)
        (generate-parens (dec open-budget)
                         (inc close-debt)
                         (str prefix \()))
      (when (pos? close-debt)
        (generate-parens open-budget
                         (dec close-debt)
                         (str prefix \))))))))

(generate-parens 3)
;; => ("((()))" "(()())" "(())()" "()(())" "()()()")


(defn parens
  "super concise soultion by @sophiebits
  https://twitter.com/sophiebits/status/1249634018057834502
  (translated from javascript)"
  [n]
  (if (zero? n)
    [""]
    (for [i (range n)
          left (parens i)
          right (parens (- n i 1))]
      (str \( left \) right))))

(parens 3)

;; same solution as before but without passing prefix to nested call
(defn generate-parens-no-prefix
  ([n]
   (generate-parens-no-prefix n 0))
  ([open-budget close-debt]
   (if (and (zero? open-budget)
            (zero? close-debt))
     [""]
     (lazy-cat
      (when (pos? open-budget)
        (map #(str % \))
             (generate-parens-no-prefix (dec open-budget) (inc close-debt))))
      (when (pos? close-debt)
        (map #(str % \()
             (generate-parens-no-prefix open-budget (dec close-debt))))))))

(generate-parens-no-prefix 3)
