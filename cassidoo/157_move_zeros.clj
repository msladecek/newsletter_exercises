(defn move-zeros [nums]
  (loop [nums-out []
         nums-in nums
         zero-count 0]
    (if (not (seq nums-in))
      (concat nums-out (repeat zero-count 0))
      (let [fst (first nums-in)]
        (if (zero? fst)
          (recur nums-out (next nums-in) (inc zero-count))
          (recur (conj nums-out fst) (next nums-in) zero-count))))))

(move-zeros [1 2 0 1 0 0 3 6])



(defn move-zeros-transducer [reducing-func]
  (let [zero-count (volatile! 0)]
    (fn
      ([] (reducing-func))
      ([result] (reduce reducing-func result (repeat @zero-count 0)))
      ([result num]
       (if (zero? num)
         (vswap! zero-count inc)
         (reducing-func result num))))))

(sequence move-zeros-transducer [1 2 0 1 0 0 3 6])
