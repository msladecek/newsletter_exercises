(defn minimum-of-pivoted [arr]
  (let [left (quot (count arr) 4)
        mid (quot (count arr) 2)
        right (+ mid left)]
    (cond
      (< (arr left) (arr mid)) (minimum-of-pivoted (subvec arr 0 mid))
      (> (arr right) (arr mid)) (minimum-of-pivoted (subvec arr mid))
      :else mid)))


(minimum-of-pivoted [4 100 0 1 2 3]) ; => 0
