(require '[clojure.math.combinatorics :as combo])


(defn special-pairs [arr]
  (let [located-arr (map #(-> {:location %1 :value %2}) (range) arr)
        value-groups (group-by :value located-arr)]
    (reduce + (map #(combo/count-combinations % 2) (vals value-groups)))))


(special-pairs [1 2 3 1 1 3]) ; => 4
