(defn max-range [numbers]
  (- (apply max numbers) (apply min numbers)))

(max-range [2 2 1 3])
(max-range [0])
(max-range [4 3 5 7 1 7 7 6])
