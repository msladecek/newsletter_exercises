(require '[clojure.string :as str]
         '[clojure.edn :as edn]
         '[clojure.alpha.spec :as s])

(defn baby-lisp [body]
  (eval
   `(let [~'add +
          ~'subtract -
          ~'multiply *
          ~'divide /]
     ~(read-string body))))


(baby-lisp "(add 1 2)") ;; => 3
(baby-lisp "(multiply 4 (add 2 3))") ;; => 20


(defn split [body]
  (-> body
      (str/replace #"[()]" #(str " " % " " ))
      (str/split #" ")
      (->> (filter #(not= "" %)))))

(s/def ::paren-open #{"("})
(s/def ::paren-close #{")"})
(s/def ::operator #{"add" "subtract" "multiply" "divide"})
(s/def ::argument (s/or :scalar #(integer? (try (edn/read-string %)
                                                (catch Exception e nil)))
                        :subexpr (s/cat :operator ::operator
                                        :arg1 ::argument
                                        :arg2 ::argument)))
(s/def ::form (s/cat :paren-open ::paren-open
                     :operator ::operator
                     :arg1 ::argument
                     :arg2 ::argument
                     :paren-close ::paren-close))
(s/def ::parser (s/cat :prefix (s/* string?)
                       :paren-open ::paren-open
                       :operator ::operator
                       :arg1 ::argument
                       :arg2 ::argument
                       :paren-close ::paren-close
                       :postfix (s/* string?)))


(defmulti unpack-argument first)

(defmethod unpack-argument :scalar [expr]
  (second expr))

(defmethod unpack-argument :subexpr [expr]
  (let [{:keys [operator arg1 arg2]} (second expr)]
    [operator (unpack-argument arg1) (unpack-argument arg2)]))


(defn parse [tokens]
  (loop [{:keys [prefix postfix operator arg1 arg2]} (s/conform ::parser tokens)]
    (let [expr [operator (unpack-argument arg1) (unpack-argument arg2)]]
      (if (and (nil? prefix) (nil? postfix))
        expr
        (recur (s/conform ::parser (concat prefix [expr] postfix)))))))


(defn compute [ast]
  (if (string? ast)
    (edn/read-string ast)
    (let [[operator arg1 arg2] ast]
      (({"add" + "multiply" * "subtract" - "divide" /} operator) (compute arg1) (compute arg2)))))


(defn baby-lisp2
  "Same functionality as `baby-lisp` but without relying on clojure's lispiness"
  [body]
  (-> body split parse compute))


(baby-lisp2 "(add 1 2)") ;; => 3
(baby-lisp2 "(multiply 4 (add 2 3))") ;; => 20
