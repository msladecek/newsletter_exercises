(require '[clojure.string :as str])

(defn html-validator [code]
  (loop [prev-code nil
         code code]
    (cond
      (zero? (count code)) true
      (= prev-code code) false
      :else (recur code
                   (-> code
                       (str/replace #"<[A-Za-z0-9]+ />" "")
                       (str/replace #"(?<=>)[^<>]+(?=<)" "")
                       (str/replace #"<([A-Za-z0-9]+)></\1>" ""))))))



(html-validator "<tag>I love coding <Component />!</tag>") ;; => true
(html-validator "<tag> <hello> I love coding <Component />!</tag>") ;; => false
(html-validator "<tag> <hello /> I love coding <Component />!</tag>") ;; => true
(html-validator "<tag> <hello /> I love coding <Component />!") ;; => false
