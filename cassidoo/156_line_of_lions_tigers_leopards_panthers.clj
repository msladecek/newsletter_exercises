(require '[criterium.core :as cc])

(defn find-non-zero [counts]
  (->> counts
       (filter (fn [[k v]] (not (zero? v))))
       (map (fn [[k v]] k))))


(defn valid-orders-count
  ([counts]
   (valid-orders-count counts nil))
  ([counts previous-selection]
   (if (every? zero? (vals counts))
     1
     (let [possible-selections (-> counts
                                   (dissoc previous-selection)
                                   find-non-zero)]
       (reduce + (map #(valid-orders-count (update counts % dec) %)
                      possible-selections))))))

#_(cc/quick-bench
 (valid-orders-count {:a 5 :b 5 :c 5 :d 5}))

(cc/quick-bench
 (valid-orders-count {:a 5 :b 5 :c 5}))

(cc/quick-bench
 (valid-orders-count {:a 5 :b 5}))

(valid-orders-count {:a 50 :b 50 :c 50})
