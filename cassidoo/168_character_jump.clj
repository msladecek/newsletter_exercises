(defn character-jump [step obstacles]
  (every? zero? (take-nth step obstacles)))

(character-jump 3 [0,1,0,0,0,1,0])
(character-jump 4 [0,1,1,0,1,0,0,0,0])
