(defn power-of-two? [num]
  (cond
    (> num 2) (power-of-two? (/ num 2))
    (= num 2) true
    (< num 2) false))
