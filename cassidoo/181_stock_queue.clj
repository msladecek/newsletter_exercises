(defn stock-queue [snapshot]
  (loop [rev-snapshot (reverse snapshot)
         out []
         visited #{}]
    (let [fst (first rev-snapshot)]
      (cond
        (nil? fst)
        (reverse out)

        (visited (:sym fst))
        (recur (rest rev-snapshot)
               out
               visited)

        :else
        (recur (rest rev-snapshot)
               (conj out fst)
               (conj visited (:sym fst)))))))


(def snapshot
  [{:sym "GME" :cost 280}
   {:sym "PYPL" :cost 234}
   {:sym "AMZN" :cost 3206}
   {:sym "AMZN" :cost 3213}
   {:sym "GME" :cost 325}])


(= (stock-queue snapshot)
   [{:sym "PYPL" :cost 234}
    {:sym "AMZN" :cost 3213}
    {:sym "GME" :cost 325}])
