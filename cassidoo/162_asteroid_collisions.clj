(defn collide [asteroids]
  (loop [left []
         right (apply list asteroids)]
    (let [left-rock (peek left)
          right-rock (peek right)]
       (cond
         ;; no more rocks to consume, return what has survived
         (nil? right-rock) left

         ;; consuming first rock
         (nil? left-rock) (recur (conj left right-rock) (pop right))

         ;; previous rock just passes by to the left
         (neg? left-rock) (recur (conj left right-rock) (pop right))

         ;; previous rock is moving to the right
         :else
         (if (pos? right-rock)
           ;; next rock is also moving to the right
           (recur (conj left right-rock) (pop right))

           ;; previous rock is moving to the right and next rock is moving to the left
           (let [collision (+ left-rock right-rock)]
             ;; surviving rock gets put to the right so it can participate in more collisions
             (cond
               ;; left rock is bigger, so it survives
               (pos? collision) (recur (pop left) (-> right pop (conj left-rock)))

               ;; right rock is bigger, so it survives
               (neg? collision) (recur (pop left) right)

               ;; rocks are the same size, so they're both destroyed
               :else (recur (pop left) (pop right)))))))))


(collide [5 8 -5]) ; => [5 8]
(collide [5 -8 -5]) ; => [-8 -5]
(collide [10 -10]) ; => []
(collide [10 -1 -10]) ; => []
(collide [10 1 -10]) ; => []
