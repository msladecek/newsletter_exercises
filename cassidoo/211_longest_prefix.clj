(defn longest-prefix [words]
  (->> words
       (#(apply map vector %))
       (take-while #(apply = %))
       (map first)
       (apply str)))


(longest-prefix ["cranberry" "crawfish" "crap"]) ; => cra
(longest-prefix ["parrot" "poodle" "fish"]) ; => ""
