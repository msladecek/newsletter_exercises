(defn neighbours [[x y]]
  (for [xx [(dec x) x (inc x)]
        yy [(dec y) y (inc y)]]
    [xx yy]))


(defn generate-minesweeper [grid-size mines]
  (let [mines (into #{} mines)
        indicators (->> mines
                        (mapcat neighbours)
                        frequencies)]
    (for [x (range grid-size)]
      (for [y (range grid-size)]
        (let [pos [(inc y) (inc x)]]
          (cond
            (mines pos) :mine
            (indicators pos) (indicators pos)
            :else :empty))))))


(defn print-minesweeper [grid]
  (doseq [row grid]
    (println (->> row
                  (map #(get {:mine \* :empty \x} % %))
                  (apply str)))))


(print-minesweeper (generate-minesweeper 5 [[1, 3], [3, 5], [2, 4]]))
