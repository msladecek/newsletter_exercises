(defn array-diff [nums target]
  (let [set-nums (set nums)]
    (count (filter #(set-nums (+ target %)) nums))))


(array-diff [1 2 3 4] 1) ; => 3
