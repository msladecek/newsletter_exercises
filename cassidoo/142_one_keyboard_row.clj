(defn word-matches-layout [rows word]
  (some (fn [row-chars]
          (every? row-chars word))
        rows))
(defn one-row
  ([words]
   (one-row "iso" words))
  ([layout words]
   (let [layouts {"iso" [(set "`1234567890-=")
                         (set "qwertyuiop[]")
                         (set "asdfghjkl;'\\ASDFGHJKL")
                         (set "zxcvbnm,./ZXCVBNM<>?")]
                  "ansi" [(set "`1234567890-=")
                          (set "qwertyuiop[]\\")
                          (set "asdfghjkl;'ASDFGHJKL")
                          (set "zxcvbnm,./ZXCVBNM<>?")]
                  "dvorak" [(set "`1234567890[]")
                            (set "',.pyfgcrl/=\\")
                            (set "aoeuidhtns-AOEUIDHTNS")
                            (set ",qjkxbmwvz:QJKXBMWVZ")]
                  "colemak" [(set "`1234567890-=")
                             (set "qwfpgjluy;[]\\")
                             (set "arstdhneio'")
                             (set "zxcvbkm,./ZXCVBKM<>?")]}
         selected-rows (layouts layout)
         word-matches-selected-layout #(word-matches-layout selected-rows %)]
     (filter word-matches-selected-layout words))))


(one-row ["candy", "doodle", "pop", "shield", "lag", "typewriter"])
