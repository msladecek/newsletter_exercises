(require '[clojure.math.combinatorics :as combo])

(defn valid-segment? [segment]
  (and (pos? (count segment))
       (<= 0 (Integer/parseInt segment) 255)))

(defn get-ip [string]
  (let [segment-count 4]
    (for [segment-lengths (apply combo/cartesian-product (repeat (dec segment-count) (range 1 4)))
          :let [splits-inner (reductions + segment-lengths)
                splits-outer (concat [0] splits-inner [(count string)])
                segments (for [[start end] (partition 2 1 splits-outer)]
                           (subs string start end))]
          :when (and (< (last splits-inner) (count string))
                     (every? valid-segment? segments))]
      (apply str (interpose \. segments)))))

(get-ip "11211")
