(defn decimal->fraction [n]
  (let [n (bigdec n)
        scale (.scale n)
        numer (bigint (.scaleByPowerOfTen n scale))
        denom (bigint (.scaleByPowerOfTen 10M (dec scale)))]
    (/ numer denom)))


(decimal->fraction 10002.12122)
