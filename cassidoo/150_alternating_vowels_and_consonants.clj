(require '[clojure.test :refer [is deftest]])

(defn letter-kind [ch]
  (cond
    ((set "aeiouAEIOU") ch) :vowel
    ((set "bcdfghjklmnpqrstuvwxyzBCDFGHJKLMNPQRSTUVWXYZ") ch) :consonant))

(defn most-valuable-char [chars]
  (apply max-key int chars))

(defn alternate-chars [string]
  (->> string
       (partition-by letter-kind)
       (map most-valuable-char)
       clojure.string/join))

(deftest test-alternate-chars
  (is (= "ababab" (alternate-chars "ababab")))
  (is (= "casiyisaner" (alternate-chars "cassidyisanerd"))))
