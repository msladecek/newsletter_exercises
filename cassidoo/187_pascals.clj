(defn pascals-recursive [row-index]
  (if (zero? row-index)
    [1]
    (let [prev-pascals (pascals-recursive (dec row-index))
          pairs (partition 2 1 (conj prev-pascals 0))]
      (into [1] (map #(apply + %) pairs)))))


(defn factorial [num]
  (reduce * (range 1 (inc num))))


(defn pascals-binomial [row-index]
  (let [numerator (factorial row-index)]
    (mapv #(/ numerator (* (factorial %)
                           (factorial (- row-index %))))
          (range 0 (+ 1 row-index)))))


(pascals-recursive 0) ;=> [1]
(pascals-recursive 3) ;=> [1 3 3 1]


(pascals-binomial 0) ;=> [1]
(pascals-binomial 3) ;=> [1 3 3 1]


(doseq [row-id (range 10)]
  (let [row (pascals-recursive row-id)]
    (println row)))

(doseq [row-id (range 10)]
  (let [row (pascals-binomial row-id)]
    (println row)))
