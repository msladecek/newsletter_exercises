(require '[clojure.string :as str])


(defn find-sentences [letters words]
  (let [matching-words (filter #(str/starts-with? letters %) words)]
    (if (seq matching-words)
      (mapcat (fn [word]
             (let [tails (find-sentences (subs letters (count word)) words)]
               (map #(into [word] %) tails)))
           matching-words)
      [letters])))


(->> (find-sentences "penpineapplepenapple" ["apple" "pen" "applepen" "pine" "pineapple"])
     (mapv #(apply str (interpose " " %))))
; => [ "pen pine apple pen apple", "pen pineapple pen apple", "pen pine applepen apple" ]
